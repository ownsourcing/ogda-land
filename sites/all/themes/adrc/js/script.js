/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    // Beginning of custom js.

// Make sure JS is on before you style everything for it.
$(function() {
 $('body').addClass('js-is-on');
});

// adds some styling for intro paragraphs on long pages. If there are more than 4 P tags in the maincontent, the first one that isn't in the header gets a class of intro. 

if ($(".maincontent article p").length > 3) {
  $(".maincontent article p").not("header p").first().not(".plain").addClass("intro"); /* hides 4, 5, 6... */
}

// left side drawer navigation (no js gets you an inverted L)


$(function() {
    if ($('html').has($('.region-localnav')).length) {
            }
        else {
            $('.pagewrapper').before('<div class="drawertoggle drawershow backbutton">Back</div>');
            $('.backbutton').click(function(){
            parent.history.back();
            return false;
        });
        }
    });

$(function() {
    $('.region-localnav .navwrapper ul').prepend('<div class="drawerclose stickdiv">Close</div>').addClass('slideout');
    $('.region-localnav .navwrapper ul').hide();
    $(".drawertoggle, .drawerclose").click(function() {
        $('.region-localnav .navwrapper ul').toggle('slide','swing');
        $(window).resize();
    });
    $(".region-localnav .navwrapper ul").mouseleave(function(){
        navdrawerTimeout = setTimeout(function(){
                $(".region-localnav .navwrapper ul").hide('slide');
                $(".drawershow").show('slide');
            },1500);
            $(".region-localnav .navwrapper ul").mouseenter(function(){
            clearTimeout(navdrawerTimeout);
            });
        }); 
});



/* FILTER BOX */

$(function() {
    $(".profilelisting").prepend('<div class="filterboxwrapper"><input type="text" id="filterbox" value="" placeholder="start typing to filter"><br /><p class="notfoundmessage"><em>Sorry, nothing matched your search</p></div>');
    $(".notfoundmessage").hide();
        $('#filterbox').keyup(function(){
        var valThis = $(this).val().toLowerCase();
        var noresult = 0;
        if(valThis == ""){
            noresult = 1;
        } else {
            $('.resultscard').each(function(){
                var text = $(this).text().toLowerCase();
                var match = text.indexOf(valThis);
                if (match >= 0) {
                    $(this).fadeIn(200, "swing");
                    noresult = 1;
                } else {
                    $(this).fadeOut(300, "swing");
                }
            });
        };
        if (noresult === 0) {
        $(".notfoundmessage").show();
    }
        if (noresult != 0) {
        $(".notfoundmessage").hide();
    }
         
    });
});

    

/* Stickydiv, keeps the "menu" button stuck to the top of the page after you scroll down */
$(function() {
var s = $(".drawershow");
    if (typeof s.position() !== 'undefined') {
        var pos = s.position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top) {
                s.addClass("stickdiv ");
            } else {
                s.removeClass("stickdiv ");
            }
        });
    }
});

// accordion. what else could this be? needs jquery ui

$(function() {
    $( ".accordion" ).accordion(
        {header: "dt,h2,h3,strong" },
        { heightStyle: "content" },
        { animate: 200 }
    );
});

 // Dropdown functionality for breadcrumb (no js turns the dropdown off. Someday let's do this with CSS only, for now it's a progressive enhancement)

$(function() {

    $( ".breadcrumb .navwrapper nav li a" ).mouseover(function() {
      $(this).closest('ul').addClass( "showit" );
    });
    $( ".breadcrumb .navwrapper" ).mouseout(function() {  //there's gotta be a better way to do this, but this is what I gots right now
      $( ".showit").removeClass("showit" );
    });

});
 
 // GlobalFooter Columnizing

    $('.region-globalfooter div').columnize({width: 200});

/* Accordion for Global Footer */


        $(".region-globalfooter h3").next().hide();
        $(".region-globalfooter h3").click(function(){
            if (!$(this).hasClass('active')) {
                $(".active").removeClass("active");
                $( this ).addClass("active");
                $(".open").removeClass("open").slideUp("fast");
                $( this ).next("div").addClass("open");
                $(".open").slideToggle("slow");
                $(".globalfooterbasics").hide();
                }
             else {
                $(".active").removeClass("active");
                $(".open").removeClass("open").slideUp("fast");
                $(".globalfooterbasics").show();
            }
        });

  // Globalheader Sliding (no JS just leaves it open)


$(function() {
    $('.region-globalheader').prepend('<span class="globalNavToggle open" style="display: block;">Columbia University Medical Center</span><span class="globalNavToggle" style="display: none;">CUMC</span>');
    $('.region-globalheader ul li').hide();
    $(".globalNavToggle").click(function() {
        $( ".globalNavToggle.open" ).hide();
        $( ".globalNavToggle" ).toggleClass('open');
        $( ".globalNavToggle.open" ).toggle("slide");
        $( ".region-globalheader ul li" ).toggle("slide");
    });

});

  // end of custom js. 

  }
};


})(jQuery, Drupal, this, this.document);
