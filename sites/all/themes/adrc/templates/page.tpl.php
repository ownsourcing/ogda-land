<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>



<!-- Global Header -->
  <?php print render($page['globalheader']); ?>
  
<!-- End Global Header -->
<!-- Site Header -->
 
<section class="siteheader">  
  <?php if ($site_name): ?>
    <div class="logo-wrap">
      <?php if ($site_name): ?>
        <h1 class="logotype ir gray">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
        </h1>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  <?php print render($page['siteheader']); ?>
</section>
<!-- End Site Header -->

<?php print render($page['top']); ?>

      <?php
        // Render the sidebars to see if there's anything in them.
        $localnav  = render($page['localnav']);
      ?>
      <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php 
      global $user; 
      if($user->uid)
        {
        print render($tabs);

        }
      ?>


      <?php if ($localnav): ?>
      <div class="drawertoggle drawershow">More in <?php print $title; ?></div>
          <?php print $localnav; ?>
      <?php endif; ?>

      <div class="pagewrapper">
        <?php print render($page['highlighted']); ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        
       
        <div class="breadcrumb">
          <?php
            $block = module_invoke('menu_block', 'block_view', 3);
            print render($block['content']);
          ?>
        </div>
        


        <?php if ($title): ?>
        
          <h1 class="page__title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>
        
        <?php print render($page['feature']); ?>
        <?php print render($page['mainpagecontent']); ?>
        <?php print $feed_icons; ?>

    <?php
      // Render the first sidebar to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
    ?>

    <?php if ($sidebar_first): ?>
        <?php print $sidebar_first; ?>
    <?php endif; ?>
    <?php
      // Render the second sidebar to see if there's anything in them.
      $sidebar_second = render($page['sidebar_second']);
    ?>
    <?php if ($sidebar_second): ?>
      <aside class="sidebars">
        <?php print $sidebar_second; ?>
      </aside>
    <?php endif; ?>
  </div> 
  </section>

  <?php print render($page['footer']); ?>




  <?php print render($page['globalfooter']); ?>

</section>

<?php print render($page['bottom']); ?>
