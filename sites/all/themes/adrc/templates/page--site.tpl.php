<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>


<!-- Global Header -->
<?php print render($page['globalheader']); ?>
<!-- End Global Header -->

<!-- Site Header --> 
<section class="siteheader">  
    <?php if ($site_name): ?>
        <div class="logo-wrap">
            <?php if ($site_name): ?>
                <h1 class="logotype ir gray">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php
        print render($page['siteheader']);
    ?>
</section>
<!-- End Site Header -->

<!-- ADRC background image on front page-->
<div class="banner-front">
    <div class="site-head" id="site-head">
        <?php if ($site_name): ?>
            <?php if ($title): ?>
                <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                    <h1><?php print $site_name; ?></h1>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php print render($page['site_head']); ?>
    </div>

    <!-- ADRC Quote or teaser text under page title -->
    <div class="site-quote">
        <?php print render($page['site_quote']); ?>
    </div>
    <!-- end Quote -->
</div>
<!-- end background image --> 


<!--maincontent--> 
<section class="maincontent landing">

    <!-- left side bar on front page for ADRC quick links, news, events -->
    <section class="sidebar sidebar-front">
        <?php print render($page['sidebar_front']);?>
    </section>
    <!-- end left sidebar -->

    <?php print render($page['mainpagecontent']); ?>

</section>
<!-- End maincontent --> 

<div class="clear"></div>
            
<section class="footer">
  <?php print render($page['footer']); ?>
  <?php print render($page['globalfooter']); ?>
</section>

<?php print render($page['bottom']); ?>
