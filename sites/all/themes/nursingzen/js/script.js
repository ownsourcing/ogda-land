/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.nursingzen = {
    attach: function (context, settings) {

      if (settings.nursingzen === undefined) {
        settings.nursingzen = {};
      }

      // Theme templates
      Drupal.theme.prototype.nursingzenNavtoggle = function () {
        return "<img src='" + settings.nursingzen.themePath + "/img/navtoggle.png'>" + Drupal.t('Menu');
      };
      Drupal.theme.prototype.nursingzenSubNavtoggle = function () {
        return "<img src='" + settings.nursingzen.themePath + "/img/navtoggle.png'>" + Drupal.t('Sub Menu');
      };
      Drupal.theme.prototype.nursingzenGlobalNavtoggle = function () {
        var span = $('<span/>', { 'class': 'globalNavToggle' });
        abbr = span.clone().hide().append(Drupal.t('Hide'));
        full = span.clone().addClass('open').css({ 'display': 'block' })
          .append(Drupal.t('Columbia University Medical Center'));
        return full.prop('outerHTML') + abbr.prop('outerHTML');
      };
      Drupal.theme.prototype.nursingzenFilterbox = function () {
        var input = $('<input/>', {
          type: 'text',
          id: 'filterbox',
          name: 'filterbox',
          value: '',
          placeholder: Drupal.t('Start typing to filter')
        });
        var label = $('<label/>', { 'for': 'filterbox' })
          .append(Drupal.t('Filter')).hide();
        return $(label).prop('outerHTML') + $(input).prop('outerHTML');
      }

      // Make sure JS is on before you style everything for it.
      // Use jquery once to ensure event is only fired once on load.
      // Once will add the first argument as a class with the suffix
      //   '-processed' so it knows not to process is again, i.e.
      //   after completion body will have class 'js-processed'
      $('body', context).once('js', function() {
        $(this).addClass('js-is-on');

        // Globalheader Sliding (no JS just leaves it open)
        var region = $(this).find('.region-globalheader'),
            lis = region.find('ul li');

        region.prepend(Drupal.theme('nursingzenGlobalNavtoggle'));
        lis.hide();

        region.find('.globalNavToggle').click(function() {
          $('.globalNavToggle.open').hide();
          $('.globalNavToggle').toggleClass('open');
          $('.globalNavToggle.open').slideToggle("fast");
          lis.slideToggle('fast');
        });
      });

      // Columnizing
      // Only execute if element exists in loaded document (context)
      $('.breadcrumbmore > ul.menu', context).once('columnize', function () {
        $(this).columnize({ width: 300});
      });
      $('.region-globalfooter nav.cumc-footer-menu ul.menu', context).once('columnize', function () {
        $(this).columnize({ columns: 5 });
      });

      // Intro paragraphs
      // Adds intro class to first paragraph if maincontent has > 4 p tags
      // Do not add styling to first paragraph if class == 'plain'
      // cckeditor.js not picking up "styles" setting, otherwise, working.
      $('.maincontent article', context).once('paragraph-style', function () {
        var paragraphs = $(this).find('.field-body p');
        if ($(paragraphs).length > 3) {
          $(paragraphs).first().not('.plain').addClass('intro');
        }
      });


      // Add navigation toggle to menus for small screens
      function addNavtoggle(container, options) {
        if (container === undefined) return;
        var toggleSettings = {
          classes: 'navtoggle',
          theme: 'nursingzenNavtoggle'
        };
        $.extend(toggleSettings, options);
        var menu = $(container).find('ul.menu'),
            toggle = $('<a/>', { 'class': toggleSettings.classes });
        toggle.prepend(Drupal.theme(toggleSettings.theme)).prependTo($(container));
        toggle.click(function () {
          menu.slideToggle('fast');
          $(container).toggleClass('active');
        });
      }
      
      // $('.menu-block-5 ul').hide();
      // $('.persistentnav ul').hide();

      $('nav.persistentnav', context).once('navtoggle', function() {
        addNavtoggle(this);
      });
      $('.breadcrumbmore', context).once('sub-navtoggle', function() {
        addNavtoggle(this, {
          classes: 'sub-navtoggle',
          theme: 'nursingzenSubNavtoggle'
        });
      });


      // Accordion, e.g. on Admissions FAQ. Using jQuery UI Accordion plugin
      //   provided by Drupal core/jquery_update module.
      if ($.ui.accordion) {
        $('.accordion', context).once('accordion', function () {
          $(this).accordion({
            header: 'dt,h2.h3,strong',
            heightStyle: 'content',
            animate: 200
          });
        });
      }


      // Toggle for Drupal Login Form
      // Only execute if element exists in loaded document (context)
      $('#user-login', context).once('user-login', function () {
        $('#edit-wind').addClass('button');
        var formdivs = $(this).find('div div');
        var toggleopen = Drupal.t('CUMC web services login')
        var toggleclosed = Drupal.t('Nevermind, hide that!')
        $(formdivs).hide();
        var button = $('<span/>', { 'class': 'login-toggle-button button' });
        $(button).html(toggleopen).appendTo(this);
        $(button).toggle(function() {
          $(formdivs).slideToggle();
          $(button).html(toggleclosed);
        }, function() {
          $(button).html(toggleopen);
          $(formdivs).slideToggle();
        });
      });



      // Search filter, for faculty listings
      $('.profilelisting', context).once('search-filter', function() {
        var form = $('<form/>', { 'class': 'filterboxwrapper' }),
            filterbox = Drupal.theme('nursingzenFilterbox'),
            message = $('<p/>', { 'class': 'notfoundmessage' })
              .append(Drupal.t('Sorry, nothing matched your search'));
        form.submit(function(e) {
          e.preventDefault();
        });
        form.prepend(filterbox).prependTo($(this));
        form.find('input').keyup(function() {
          var valThis = $(this).val().toLowerCase();
          var noresult = 0;
          if ( valThis == '') {
            noresult = 1;
          } else {
            $('.resultscard').each(function() {
              var text = $(this).text().toLowerCase();
              var match = text.indexOf(valThis);
              if (match >= 0) {
                $(this).fadeIn(200, "swing");
                noresult = 1;
              } else {
                $(this).fadeOut(300, "swing");
              }
            });
          }
          // Only add message if there are no results
          if (noresult === 0) {
            message.appendTo(form);
          }
          if (noresult != 0) {
            message.remove();
          }
        });
      });



    }
  };
})(jQuery, Drupal, this, this.document);

