<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */

/**
 * Implements hook_theme().
 *
 */
function nursingzen_theme($existing, $type, $theme, $path) {
  return array(
    'nursingzen_byline' => array(
      'render element' => 'element',
      'variables' => array(
        'given_name' => NULL,
        'family_name' => NULL,
        'phone_number' => NULL,
        'email' => NULL
      ),
      'template' => 'nursingzen-byline',
      'path' => drupal_get_path('theme', 'nursingzen') . '/templates',
      'pattern'   => 'nursingzen_byline__'
    )
  );
}

/**
 * Implements hook_css_alter().
 */
function nursingzen_css_alter(&$css) {
  // Remove unwanted CSS
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css['misc/ui/jquery.ui.accordion.css']);
}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function nursingzen_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  nursingzen_preprocess_html($variables, $hook);
  nursingzen_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function nursingzen_preprocess_html(&$variables, $hook) {
  drupal_add_library('system', 'ui'); 
  drupal_add_library('system', 'ui.accordion'); 
  // pass theme path to js
  drupal_add_js(array('nursingzen' => array(
    'themePath' => base_path() . path_to_theme()
  )), 'setting');
  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function nursingzen_preprocess_page(&$variables) {
  if (isset($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }
  $variables['subtitle'] = NULL;
  if (isset($variables['node']->field_referenced_article)) {
    $articles = field_get_items('node', $variables['node'], 'field_referenced_article');
    // There should only be one referenced article so we grab the first
    $article = array_shift($articles);
    // Promote sub title field value to own variable for use in page template
    if (isset($article['entity']->field_sub_title)) {
      $variables['subtitle'] = field_view_field('node', $article['entity'], 'field_sub_title', array('label' => 'hidden'));
    }
  }
  // Don't use page title on CUPS profile pages
  // @todo Make CUPS profile data available to page e.g. using menu_get_object()
  //       so we can override title rather than removing it.
  if (($menu_item = menu_get_item()) && $menu_item['page_callback'] == 'cups_facultyprofile_view') {
    $variables['title'] = '';
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function nursingzen_preprocess_node(&$variables, $hook) {
  // To use dsm (see https://www.drupal.org/node/1422072):
  // global $preprocess_node_dsm;
  // $preprocess_node_dsm = 1;

  // Optionally, run node-type-specific preprocess functions, like
  // nursingzen_preprocess_node_page() or nursingzen_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
  // Build byline from media contact
  $variables['byline'] = NULL;
  if (isset($variables['field_media_contact'])) {
    $wrapper = entity_metadata_wrapper('node', $variables['node']);
    $contact = $wrapper->field_media_contact->value();
    if (!empty($contact)) {
      $field_names = array('field_given_name', 'field_family_name', 'field_phone_number');
      $content = array();
      foreach ($field_names as $name) {
        $key = str_replace('field_', '', $name);
        $item = field_view_field('user', $contact, $name, array('label' => 'hidden'));
        $content[$key] = drupal_render($item);
      }
      $content['phone_number'] = preg_replace('/[^0-9]/', '', $content['phone_number']);
      $content['phone_number'] = preg_replace('/^([0-9]{3})([0-9]{3})([0-9]*)$/', '($1) $2-$3', $content['phone_number']);
      $content['email'] = $contact->mail;
      $variables['byline'] = theme('nursingzen_byline', $content);
    }
  }
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function nursingzen_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function nursingzen_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--no-wrapper.tpl.php template for content.
  // Edit module expects content region classes.
  if ($variables['region'] == 'content') {
    $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__no_wrapper'));
  }
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function nursingzen_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

/**
 * Implements hook_preprocess_menu_block_wrapper().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("menu_block_wrapper" in this case.)
 */
function nursingzen_preprocess_menu_block_wrapper(&$variables, $hook) {
  // Add admin title to classes
  if (isset($variables['config']['admin_title']) && !empty($variables['config']['admin_title'])) {
    $variables['classes_array'][] = drupal_html_class($variables['config']['admin_title']);
  }
}

/**
 * Implements hook_preprocess_field().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("field" in this case.)
 */
function nursingzen_preprocess_field(&$variables, $hook) {
  // Add field item wrappers, required by edit module, to Fences templates.
  if (module_exists('edit') && module_exists('fences')) {
    $hooks = theme_get_registry(FALSE);
    $template = '';
    foreach (array_reverse($variables['theme_hook_suggestions']) as $suggestion) {
      if (isset($hooks[$suggestion])) {
        $template = $suggestion;
        break;
      }
    }
    if (substr($template, 0, 13) === 'field__fences') {
      foreach ($variables['items'] as &$item) {
        $item['#prefix'] = '<div class="field-item">';
        $item['#suffix'] = '</div>';
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function nursingzen_preprocess_cups_facultyprofile(&$variables) {
  // @todo We shouldn't have to do this here, instead add more theme hooks to
  //       CUPS with render elements instead of markup to make theming more
  //       flexible.
  $variables['name'] = array(
    '#type' => 'markup',
    'content' => array(
      'first' => $variables['CUPS_Person_Name_First'],
      'middle' => $variables['CUPS_Person_Name_Middle'],
      'last' => $variables['CUPS_Person_Name_Last'],
      'suffix' => $variables['CUPS_Person_Name_NameSuffix']
    )
  );
  $variables['name']['#markup'] = implode(' ', array_filter($variables['name']['content']));
  $variables['professional_suffix'] = array(
    '#type' => 'markup',
    'content' => array(
      'professional_suffix' => trim($variables['CUPS_Person_Name_ProfSuffix'])
    )
  );
  $variables['professional_suffix']['#markup'] = $variables['professional_suffix']['content']['professional_suffix'];
}
