<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?>"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title || $byline): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php
        /*
         * When rendering a node as a referenced entity we usually don't want
         * the title. Commenting this out for now, until we can find a more
         * elegant fix. Note contextual links are rendered with title suffix so
         * always shoow prefix and suffix.
         *
         * <?php if (!$page && $title): ?>
         *   <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
         * <?php endif; ?>
         */
      ?>
      <?php print render($title_suffix); ?>

      <?php if ($byline): ?>
        <div class="time-byline">
          <span class="datetime"><?php print $pubdate; ?></span> |
          <?php print render($byline); ?>
        </div>
     <?php elseif ($display_submitted): ?>
        <?php // Use display settings on content type to hide/show posted date. ?>
        <span class="datetime"><?php print $pubdate; ?></span>
     <?php endif; ?>

      <?php if ($unpublished): ?>
        <mark class="watermark"><?php print t('Unpublished'); ?></mark>
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article>
