<?php
/**
 * @file
 * cups_facultyprofile.tpl.php
 */
?>
<article class="<?php print $ArticleClassName; ?>">
  <section class="person">
    <header>
      <h1 class="page__title"><?php print render($name); ?>
        <?php if ($pro_suffix = render($professional_suffix)) : ?>
          <span><?php print $pro_suffix; ?></span>
        <?php endif; ?>
      </h1>
    </header>
    <?php if (isset($CUPS_Person_Departments)) {
      $CupsPersonDepartments = array_unique($CUPS_Person_Departments);
    }
    if(isset($CupsPersonDepartments) && count($CupsPersonDepartments) > 0): ?>
      <section class="departments">
        <h3 class="label">Departments And Divisions</h3>
        <ul>
        <?php foreach($CupsPersonDepartments as &$CupsPersonDepartment): ?>
          <li><?php print $CupsPersonDepartment; ?></li>
        <?php endforeach; ?>
        </ul>
      </section><!--departments-->
    <?php endif; ?>
    <!-------------------------------------------------------------->
    <?php if($DisplayAppointments) : ?>
    <?php
    if(isset($CUPS_Person_Titles)){
      $CupsPersonTitles = array_unique($CUPS_Person_Titles);
    }
    if(isset($CupsPersonTitles) && count($CupsPersonTitles) > 0): ?>
      <section class="titles">
        <!--<h3 class="label">Academic Appointments</h3>-->
        <ul>
        <?php foreach($CupsPersonTitles as &$CupsPersonTitle): ?>
          <li><?php print $CupsPersonTitle; ?></li>
        <?php endforeach; ?>
        </ul>
      </section><!--titles-->
    <?php endif; ?>
    <?php endif; ?>
    <!-------------------------------------------------------------->
    <?php if($DisplayTitles) : ?>
    <?php
    if(isset($CUPS_Person_CurrentAdminTitles)){
      $CupsPersonCurrentAdminTitles = array_unique($CUPS_Person_CurrentAdminTitles);
    }
    if(isset($CupsPersonCurrentAdminTitles) && count($CupsPersonCurrentAdminTitles) > 0): ?>
      <section class="positions_prof_experience">
        <!--<h3 class="label">Current Administrative Positions</h3>-->
        <ul>
        <?php foreach($CupsPersonCurrentAdminTitles as &$CupsPersonCurrentAdminTitle): ?>
          <li><?php print $CupsPersonCurrentAdminTitle; ?></li>
        <?php endforeach; ?>
        </ul>
      </section><!--admin positions-->
    <?php endif; ?>
    <?php endif; ?>
    <!-------------------------------------------------------------->
    <?php if($DisplayHeadShot && $CUPS_Person_ShowHeadshot) : ?>
    <section class="image">
      <img src="<?php print $CUPS_Person_HeadShotUrl; ?>" alt="<?php print $CUPS_Person_Name; ?>">
    </section><!--image-->
    <?php endif; ?>
    <!-------------------------------------------------------------->
    <?php if($DisplayVideo && $CUPS_Person_ShowVideo && $CUPS_Person_VideoUrl != '') : ?>
    <section class="video">
      <iframe width="560" height="315" src="<?php print $CUPS_Person_VideoUrl; ?>" frameborder="0" frameborder="0" allowfullscreen></iframe>
    </section><!--video-->
    <?php endif; ?>
    <!-------------------------------------------------------------->
  </section>
  <!-------------------------------------------------------------->
  <?php if($DisplayGender) : ?>
    <section class="gender">
      <h3 class="label">Gender:</h3>
      <span><?php print $CUPS_Person_Gender; ?></span>
    </section>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayNYTopDoctor || $DisplayAmericasTopDoctor) : ?>
  <section class="accolades">
    <?php if($DisplayNYTopDoctor) : ?>
      <img src="http://www.cumc.columbia.edu/globalimages/top-doc-02.png">
    <?php endif; ?>
    <?php if($DisplayAmericasTopDoctor) : ?>
      <img src="http://www.cumc.columbia.edu/globalimages/top-doc-01.png">
    <?php endif; ?>
  </section><!--accolades-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayBio || $DisplayClinicalOverview || $DisplayResearcherOverview) : ?>
  <section class="bio">
    <?php if(isset($CUPS_Person_Biography) && $CUPS_Person_Biography != '' && $DisplayBio): ?>
      <section class="person">
      <?php print render($CUPS_Person_Biography); ?>
      </section>
    <?php endif; ?>
    <?php if(isset($CUPS_Provider_ClinicalOverview) && $CUPS_Provider_ClinicalOverview != '' && $DisplayClinicalOverview): ?>
      <section class="clinical">
      <?php print render($CUPS_Provider_ClinicalOverview); ?>
      </section>
    <?php endif; ?>
    <?php if(isset($CUPS_Researcher_Overview) && $CUPS_Researcher_Overview != '' && $DisplayResearcherOverview): ?>
      <section class="researcher">
      <?php print render($CUPS_Researcher_Overview); ?>
      </section>
    <?php endif; ?>
  </section><!--content-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderQuote) : ?>
  <?php if(isset($CUPS_Provider_Quote) && $CUPS_Provider_Quote != ''): ?>
    <section class="provider_quote">
      <span>&ldquo;</span>
      <?php print render($CUPS_Provider_Quote); ?>
      <span class="last">&ldquo;</span>
    </section><!--provider_quote-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherQuote) : ?>
  <?php if(isset($variables['CUPS_Researcher_Quote']) && $variables['CUPS_Researcher_Quote'] != ''): ?>
    <section class="researcher_quote">
      <span>&ldquo;</span>
      <?php print render($variables['CUPS_Researcher_Quote']); ?>
      <span class="last">&ldquo;</span>
    </section><!--researcher_quote-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderSpecialties) : ?>
    <section class="stats specialties">
      <h3 class="label">Board Certifications</h3>
      <ul>
        <?php foreach($CupsProviderSpecialties as &$CupsProviderSpecialty): ?>
        <li><?php print $CupsProviderSpecialty; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats specialties-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderExpertise) : ?>
    <section class="stats aoes">
      <h3 class="label">Areas of Expertise</h3>
      <ul>
        <?php foreach($CupsProviderClinicalAoes as &$CupsProviderClinicalAoe): ?>
        <li><?php print $CupsProviderClinicalAoe; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats aoes-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayLanguages) : ?>
    <section class="stats languages">
      <h3 class="label">Languages Spoken</h3>
      <ul>
        <?php foreach($CupsPersonLanguages as &$CupsPersonLanguage): ?>
        <li><?php print $CupsPersonLanguage; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats languages-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayEducation) : ?>
  <?php
  if(isset($CUPS_Person_EducationAndTrainings))
  {
    $CupsPersonEducationAndTrainings = array_unique($CUPS_Person_EducationAndTrainings);
  }
  if(isset($CupsPersonEducationAndTrainings) && count($CupsPersonEducationAndTrainings) > 0): ?>
    <section class="stats education">
      <ul>
        <h3 class="label">Education & Training</h3>
        <?php foreach($CupsPersonEducationAndTrainings as &$CupsPersonEducationAndTraining): ?>
          <li><?php print $CupsPersonEducationAndTraining; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats education-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderPrimaryLocations || $DisplayProviderOtherLocations) : ?>
    <section class="stats providerlocations">
      <h3 class="label">Locations</h3>
      <ul>
        <?php foreach($CupsProviderLocations as &$CupsProviderLocation): ?>
        <li><?php print $CupsProviderLocation; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats providerlocations-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderPracticesAndCenters) : ?>
  <?php
  if(isset($CUPS_Provider_PracticesAndCenters)){
    $CupsProviderPracsAndCenters = array_unique($CUPS_Provider_PracticesAndCenters);
  }
  if(isset($CupsProviderPracsAndCenters) && count($CupsProviderPracsAndCenters) > 0): ?>
    <section class="stats pracandcenters">
      <h3 class="label">Practices And Centers</h3>
      <ul>
        <?php foreach($CupsProviderPracsAndCenters as &$CupsProviderPracsAndCenter): ?>
        <li><?php print $CupsProviderPracsAndCenter; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats pracsandcenters-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayProviderAffiliations) : ?>
  <?php
  if(isset($CUPS_Provider_Affiliations)){
    $CupsProviderAffiliations = array_unique($CUPS_Provider_Affiliations);
  }
  if(isset($CupsProviderAffiliations) && count($CupsProviderAffiliations) > 0): ?>
    <section class="stats provideraffiliations">
      <h3 class="label">Provider Affiliations</h3>
      <ul>
        <?php foreach($CupsProviderAffiliations as &$CupsProviderAffiliation): ?>
        <li><?php print $CupsProviderAffiliation; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats provaffiliations-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayInsurances) : ?>
    <section class="stats insurance">
      <h3 class="label">Insurance Programs</h3>
      <p>Please contact the provider's office directly to verify that your particular insurance is accepted.</p>
      <ul>
        <?php foreach($CupsProviderInsurances as &$CupsProviderInsurance): ?>
        <li><?php print $CupsProviderInsurance; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats insurance-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayPediatricsPatientsFlag) : ?>
    <?php if($SeesPediatricPatients) : ?>
      <section class="stats ispeds">
        <p>This provider sees pediatric patients</p>
      </section><!--stats pediatrics-->
    <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayAcceptingNewPatients && $DisplayProvider) { ?>
    <section class="stats newpatients">
      <?php if($AcceptingNewPatients) { ?>
        <p>This provider accepts new patients</p>
        <?php if($CUPS_Provider_AppointmentPhoneNum != '') : ?>
          <p>Appointment Phone Number: <?php print $CUPS_Provider_AppointmentPhoneNum; ?></p>
        <?php endif; ?>
      <?php }else{ ?>
        <p>This provider does not accept new patients</p>
        <?php if($CUPS_Provider_ContactPhoneNum != '') : ?>
          <p>Contact Phone Number: <?php print $CUPS_Provider_ContactPhoneNum; ?></p>
        <?php endif; ?>
      <?php } ?>
    </section><!--stats newpatients-->
  <?php } ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayLinks) : ?>
  <?php
  if(isset($CUPS_Person_LinksToOtherSites)){
    $CupsPersonLinks = array_unique($CUPS_Person_LinksToOtherSites);
  }
  
  if(isset($CupsPersonLinks) && count($CupsPersonLinks) > 0): ?>
    <section class="stats links">
      <h3 class="label">Links</h3>
      <ul>
      <?php foreach($CupsPersonLinks as &$CupsPersonLink): ?>
        <li><?php print $CupsPersonLink; ?></li>
      <?php endforeach; ?>
      </ul>
    </section><!--links-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayPrimaryLabLocations || $DisplayOtherLabLocations) : ?>
  <?php
    $CupsResearcherPrimaryLabLocations = array();
    if(isset($CUPS_Researcher_PrimaryLabLocations)){
      $CupsResearcherPrimaryLabLocations = array_unique($CUPS_Researcher_PrimaryLabLocations);
    }
    $CupsResearcherOtherLabLocations = array();
    if(isset($CUPS_Researcher_OtherLabLocations)){
      $CupsResearcherOtherLabLocations = array_unique($CUPS_Researcher_OtherLabLocations);
    }


    if((isset($CupsResearcherPrimaryLabLocations) && count($CupsResearcherPrimaryLabLocations)>0) || (isset($CupsResearcherOtherLabLocations) && count($CupsResearcherOtherLabLocations)>0)) : ?>
    <section class="stats contact">
      <h3 class="label">Lab Locations</h3>
      <?php if(count($CupsResearcherPrimaryLabLocations) > 0): ?>
        <section class="primary">
          <ul>
          <?php foreach($CupsResearcherPrimaryLabLocations as &$CupsResearcherPrimaryLabLocation): ?>
            <li><?php print $CupsResearcherPrimaryLabLocation; ?></li>
          <?php endforeach; ?>
          </ul>
        </section>
      <?php endif; ?>
      <?php if(count($CupsResearcherOtherLabLocations) > 0): ?>
        <section class="secondary">
          <ul>
          <?php foreach($CupsResearcherOtherLabLocations as &$CupsResearcherOtherLabLocation): ?>
            <li><?php print $CupsResearcherOtherLabLocation; ?></li>
          <?php endforeach; ?>
          </ul>
        </section>
      <?php endif; ?>
    </section><!--stats contact-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayContactInformation) : ?>
  <?php
  if(isset($CUPS_Person_ContactInfoList)){
    $CupsPersonContacts = $CUPS_Person_ContactInfoList;
  }
  if(isset($CupsPersonContacts) && count($CupsPersonContacts) > 0): ?>
    <section class="stats contactinfo">
                     <h3 class="label">Contact Info</h3>
                     <ul>
                            <?php foreach($CupsPersonContacts as &$CupsPersonContact): ?>
                            <li>
                                   <?php if(isset($CupsPersonContact['InfoType']) && $CupsPersonContact['InfoType'] == 'Email') { ?>
                                          Email: <a href="mailto:<?php print $CupsPersonContact['InfoValue']; ?>"><?php print $CupsPersonContact['InfoValue']; ?></a>
                                  
                                   <?php } else { ?>
                                          <?php print $CupsPersonContact['InfoType'] . ': ' . $CupsPersonContact['InfoValue']; ?>
                                   <?php } ?>
                            </li>
                            <?php endforeach; ?>
                     </ul>
              </section><!--stats-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <!--
    $variables['CUPS_Person_TeachingResponsibilities'] = $resultsArray['ProfilesList'][0]['Person']['GHActivities'];
    $variables['CUPS_Person_ComSocMem'] = $resultsArray['ProfilesList'][0]['Person']['GHActivities'];
  -->
  <!-------------------------------------------------------------->
  <?php if($DisplayPastPositions && $CUPS_Person_PastPositions != '') : ?>
    <section class="stats pastpositions">
      <h3 class="label">Past Positions</h3>
      <?php print render($CUPS_Person_PastPositions); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayPresentations && $CUPS_Person_Presentations != '') : ?>
    <section class="stats presentations">
      <h3 class="label">Presentations</h3>
      <?php print render($CUPS_Person_Presentations); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayCommunityServices && $CUPS_Person_CommunityServices != '') : ?>
    <section class="stats comservices">
      <h3 class="label">Community Service</h3>
      <?php print render($CUPS_Person_CommunityServices); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayPatents && $CUPS_Person_Patents != '') : ?>
    <section class="stats patents">
      <h3 class="label">Patents</h3>
      <?php print render($CUPS_Person_Patents); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayTeachingResponsibilities && $CUPS_Person_TeachingResponsibilities != '') : ?>
    <section class="stats teachingresp">
      <h3 class="label">Teaching Responsibilities</h3>
      <?php print render($CUPS_Person_TeachingResponsibilities); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayComSocMem && $CUPS_Person_ComSocMem != '') : ?>
    <section class="stats memberships">
      <h3 class="label">Committees/Societies/Memberships</h3>
      <?php print render($CUPS_Person_ComSocMem); ?>
    </section><!--stats-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayGHCountries) : ?>
  <?php
  if(isset($CUPS_Person_GHCountries)){
    $CupsPersonGHCountries = array_unique($CUPS_Person_GHCountries);
  }
  if(isset($CupsPersonGHCountries) && count($CupsPersonGHCountries) > 0): ?>
    <section class="stats ghcountries">
      <h3 class="label">Global Health Countries</h3>
      <ul>
        <?php foreach($CupsPersonGHCountries as &$CupsPersonGHCountry): ?>
        <li><?php print $CupsPersonGHCountry; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats ghcountries-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayGHActivities && $CUPS_Person_GHActivities != '') : ?>
    <section class="stats ghactivites">
      <h3 class="label">Global Health Activities</h3>
      <?php print render($CUPS_Person_GHActivities); ?>
    </section><!--stats ghactivites-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayHandA) : ?>
    <section class="stats honors">
      <h3 class="label">Honors & Awards</h3>
      <?php print render($CUPS_Person_HandA); ?>
    </section><!--stats honors-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherInterests) : ?>
    <section class="stats research">
      <h3 class="label">Research Interests</h3>
      <ul>
        <?php foreach($CupsResearcherInterests as $CupsResearcherInterest): ?>
                                   <li><?php print $CupsResearcherInterest; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats research interests-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayGrants) : ?>
    <section class="stats grants">
      <h3 class="label">Grants</h3>
                <?php print $CUPS_Researcher_Grants; ?>
    </section><!--stats grants-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayNIHGrants && isset($CUPS_Researcher_NIHGrants) && count($CUPS_Researcher_NIHGrants) > 0) : ?>
    <section class="stats grants">
      <h3 class="label">NIH Grants</h3>
      <ul>
        <?php foreach($CUPS_Researcher_NIHGrants as $CUPS_Researcher_NIHGrant): ?>
                                   <li><?php print $CUPS_Researcher_NIHGrant; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats grants-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if(isset($CUPS_Person_UNI) && 2<1) : ?>
    <?php $cuspLink = 'irvinginstitute.columbia.edu/cusp/person/' . $CUPS_Person_UNI . '#dept_list_widget_view'; ?>
    <section class="stats grantsandpubs">
      <h3 class="label">Grants And Publications</h3>
                <p>For more information on this person's research activity please visit <a href="http://<?php print $cuspLink; ?>" target="_blank">cusp.org</a></p>
    </section><!--stats grants-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayLabProjects) : ?>
    <section class="stats labprojects">
    <h3 class="label">Lab Projects</h3>
      <ul>
        <?php foreach($CupsResearcherLabProjects as $CupsResearcherLabProject): ?>
                                   <li><?php print $CupsResearcherLabProject; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats labprojects-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayLabMemebers) : ?>
    <section class="stats labmembers">
    <h3 class="label">Lab Members</h3>
      <ul>
        <?php foreach($CupsResearcherLabMembers as $CupsResearcherLabMember): ?>
                                   <li><?php print $CupsResearcherLabMember; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats labmembers-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherScientificResults && $CUPS_Researcher_Results != '') : ?>
    <section class="stats scientificresults">
      <h3 class="label">Scientific Results</h3>
      <?php print render($CUPS_Researcher_Results); ?>
    </section><!--stats collaborators-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherClinicalTrials && $CUPS_Researcher_Trials != '') : ?>
    <section class="stats clinicaltrials">
      <h3 class="label">Clinical Trials</h3>
      <?php print render($CUPS_Researcher_Trials); ?>
    </section><!--stats trials-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherCollaborators && $CUPS_Researcher_Collaborators != '') : ?>
    <section class="stats collaborators">
      <h3 class="label">Collaborators</h3>
      <?php print render($CUPS_Researcher_Collaborators); ?>
    </section><!--stats collaborators-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayResearcherAffiliations) : ?>
  <?php
  if(isset($CUPS_Researcher_Affiliations)){
    $CupsResearcherAffiliations = array_unique($CUPS_Researcher_Affiliations);
  }
  if(isset($CupsResearcherAffiliations) && count($CupsResearcherAffiliations) > 0): ?>
    <section class="stats researchaffiliations">
      <h3 class="label">Researcher Affiliations</h3>
      <ul>
        <?php foreach($CupsResearcherAffiliations as &$CupsResearcherAffiliation): ?>
        <li><?php print $CupsResearcherAffiliation; ?></li>
        <?php endforeach; ?>
      </ul>
    </section><!--stats resaffiliations-->
  <?php endif; ?>
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <?php if($DisplayPublications) : ?>
    <section class="stats pubs">
      <h3 class="label">Publications</h3>
      <?php print render($CUPS_Person_Publications); ?>
      <ul>
        <?php foreach($CUPS_Person_PubmedPublications as &$CUPS_Person_PubmedPublication): ?>
          <li><?php print $CUPS_Person_PubmedPublication; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php if(isset($CUPS_Person_PublicationsLink) && $CUPS_Person_PublicationsLink != '') : ?>
      <p>For a complete list of publications, please visit <a href="http://<?php print render($CUPS_Person_PublicationsLink); ?>" target="_blank">PubMed.gov</a></p>
      <?php endif; ?>
    </section><!--stats pubs-->
  <?php endif; ?>
  <!-------------------------------------------------------------->
  <div class="clear"></div>
  <!-------------------------------------------------------------->
</article>
