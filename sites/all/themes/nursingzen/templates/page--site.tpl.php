<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>



<!-- Global Header -->
  <?php print render($page['globalheader']); ?>
<!-- End Global Header -->
<!-- Site Header -->

<section class="siteheader">
  <?php if ($site_name): ?>
    <div class="logo-wrap">
      <?php if ($site_name): ?>
        <h1 class="logotype ir gray">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
        </h1>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  <?php print render($page['siteheader']); ?>
</section>
<!-- End Site Header -->

<?php print render($page['top']); ?>
<?php
        // Render the sidebars to see if there's anything in them.
        $localnav  = render($page['localnav']);
      ?>
      
      <div class="contextual-menu">
        <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          <?php 
        global $user; 
        if($user->uid)
          {
          print render($tabs);

          }
        ?>
      </div>

<section class="maincontent">
<!-- test -->
  <?php print render($page['feature']); ?>

    <section class="region-front-left">
        <?php
        echo views_embed_view('calendar', $display_id = 'block_1');
        ?>
    </section>

    <?php print render($page['content']); ?>
<!-- test -->
      <?php
      // Render the first sidebar to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
    ?>

    <?php if ($sidebar_first): ?>
        <?php print $sidebar_first; ?>
    <?php endif; ?>

</section><!--maincontent-->

    <div class="clear"></div>

</section>

  <?php print render($page['footer']); ?>
  <?php print render($page['globalfooter']); ?>

</section>

<?php print render($page['bottom']); ?>

<script src="<?php print $base_path . drupal_get_path('theme', $GLOBALS['theme']); ?>/js/calendar.js"></script>
