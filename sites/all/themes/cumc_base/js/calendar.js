(function ($) {
    $('tr.date-box').each(function(i){				
        $(this).addClass('number-' + (i+1));
    });

    $('tr.single-day').each(function(i){
        $(this).addClass('number-' + (i+1));
    });

    $('tr.date-box.number-1 td').hover(function () {
        $("tr.single-day.number-1 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-1 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.single-day.number-1 td').hover(function () {
        $("tr.single-day.number-1 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-1 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.date-box.number-2 td').hover(function () {
        $("tr.single-day.number-2 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-2 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.single-day.number-2 td').hover(function () {
        $("tr.single-day.number-2 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-2 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.date-box.number-3 td').hover(function () {
        $("tr.single-day.number-3 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-3 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.single-day.number-3 td').hover(function () {
        $("tr.single-day.number-3 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-3 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.date-box.number-4 td').hover(function () {
        $("tr.single-day.number-4 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-4 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.single-day.number-4 td').hover(function () {
        $("tr.single-day.number-4 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-4 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.date-box.number-5 td').hover(function () {
        $("tr.single-day.number-5 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-5 td").stop(true, true).delay(560).fadeOut(160);
    });

    $('tr.single-day.number-5 td').hover(function () {
        $("tr.single-day.number-5 td").eq($(this).index()).stop(true, true).fadeIn(160);
    }, function () {
        $("tr.single-day.number-5 td").stop(true, true).delay(560).fadeOut(160);
    });
})(jQuery);

$('li.date-next a').click(function () {    
    $('tr.date-box').each(function(i){ 			//repeat function on next/prev link click
        $(this).addClass('number-' + (i+1));
    });

    $('tr.single-day').each(function(i){			//repeat function on next/prev link click
        $(this).addClass('number-' + (i+1));
    });
});    