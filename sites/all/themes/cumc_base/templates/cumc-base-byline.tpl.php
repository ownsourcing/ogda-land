<?php
/**
 * @file cumc_base-byline.tpl.php
 *
 * Available variables:
 *   $given_name
 *   $family_name
 *   $phone
 *   $email
 *
 */
?>
<span class="byline">
  <strong><?php print t('Contact:'); ?></strong>
  <span class="fullname"><?php print $given_name . ' ' . $family_name; ?></span> -
  <span class="telephone"><?php print $phone_number; ?></span> -
  <a class="e-mail" href="mailto:<?php print $email; ?>"><?php print $email; ?></a>
</span>

