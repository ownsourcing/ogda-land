<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<!-- Global Header -->
<?php print render($page['globalheader']); ?>
<!-- End Global Header -->
<!-- Site Header -->
<section class="siteheader">
  <?php if ($site_name): ?>
    <div class="logo-wrap">
      <?php if ($site_name): ?>
        <h1 class="logotype ir gray">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
        </h1>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  <?php print render($page['siteheader']); ?>
</section>
<!-- End Site Header -->

<?php print render($page['top']); ?>

<div class="contextual-menu">
  <?php if ($action_links): ?>
    <ul class="action-links"><?php print render($action_links); ?></ul>
  <?php endif; ?>
  <?php print render($tabs); ?>
</div>

<?php print render($page['localnav']); ?>

<div class="pagewrapper">
  <?php print render($page['highlighted']); ?>
  <a id="main-content"></a>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h1 class="page__title"><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($subtitle): ?>
     <h2 class="page__subtitle"> <?php print render($subtitle); ?></h2>
  <?php endif; ?>

  <?php print $messages; ?>
  <?php print render($page['help']); ?>
  <?php print render($page['feature']); ?>
  <section class="maincontent">
    <?php print render($page['content']); ?>
  </section>
  <?php print $feed_icons; ?>

  <?php print render($page['sidebar_first']); ?>

  <?php // Render sidebar to variable to determine if we need wrapper markup
    $sidebar_second = render($page['sidebar_second']);
    if ($sidebar_second):
  ?>
    <aside class="sidebars">
      <?php print $sidebar_second; ?>
    </aside>
  <?php endif; ?>
</div>

<?php print render($page['footer']); ?>
<?php print render($page['globalfooter']); ?>
<?php print render($page['bottom']); ?>

