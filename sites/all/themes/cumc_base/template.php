<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */

/**
 * Implements hook_theme().
 *
 */
function cumc_base_theme($existing, $type, $theme, $path) {
  return array(
    'cumc_base_byline' => array(
      'render element' => 'element',
      'variables' => array(
        'given_name' => NULL,
        'family_name' => NULL,
        'phone_number' => NULL,
        'email' => NULL
      ),
      'template' => 'cumc_base-byline',
      'path' => drupal_get_path('theme', 'cumc_base') . '/templates',
      'pattern'   => 'cumc_base_byline__'
    )
  );
}

/**
 * Implements hook_css_alter().
 */
function cumc_base_css_alter(&$css) {
  // Remove unwanted CSS
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css['misc/ui/jquery.ui.accordion.css']);
}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function cumc_base_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  cumc_base_preprocess_html($variables, $hook);
  cumc_base_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function cumc_base_preprocess_html(&$variables, $hook) {
  drupal_add_library('system', 'ui'); 
  drupal_add_library('system', 'ui.accordion'); 
  // pass theme path to js
  drupal_add_js(array('cumc_base' => array(
    'themePath' => base_path() . path_to_theme()
  )), 'setting');
  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function cumc_base_preprocess_page(&$variables) {
  if (isset($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }
  $variables['subtitle'] = NULL;
  if (isset($variables['node']->field_referenced_article)) {
    $articles = field_get_items('node', $variables['node'], 'field_referenced_article');
    // There should only be one referenced article so we grab the first
    $article = array_shift($articles);
    // Promote sub title field value to own variable for use in page template
    if (isset($article['entity']->field_sub_title)) {
      $variables['subtitle'] = field_view_field('node', $article['entity'], 'field_sub_title', array('label' => 'hidden'));
    }
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function cumc_base_preprocess_node(&$variables, $hook) {
  // To use dsm (see https://www.drupal.org/node/1422072):
  // global $preprocess_node_dsm;
  // $preprocess_node_dsm = 1;

  // Optionally, run node-type-specific preprocess functions, like
  // cumc_base_preprocess_node_page() or cumc_base_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
  // Build byline from media contact
  $variables['byline'] = NULL;
  if (isset($variables['field_media_contact'])) {
    $wrapper = entity_metadata_wrapper('node', $variables['node']);
    $contact = $wrapper->field_media_contact->value();
    if (!empty($contact)) {
      $field_names = array('field_given_name', 'field_family_name', 'field_phone_number');
      $content = array();
      foreach ($field_names as $name) {
        $key = str_replace('field_', '', $name);
        $item = field_view_field('user', $contact, $name, array('label' => 'hidden'));
        $content[$key] = drupal_render($item);
      }
      $content['phone_number'] = preg_replace('/[^0-9]/', '', $content['phone_number']);
      $content['phone_number'] = preg_replace('/^([0-9]{3})([0-9]{3})([0-9]*)$/', '($1) $2-$3', $content['phone_number']);
      $content['email'] = $contact->mail;
      $variables['byline'] = theme('cumc_base_byline', $content);
    }
  }
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function cumc_base_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function cumc_base_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--no-wrapper.tpl.php template for content.
  // Edit module expects content region classes.
  if ($variables['region'] == 'content') {
    $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__no_wrapper'));
  }
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function cumc_base_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

/**
 * Implements hook_preprocess_menu_block_wrapper().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function cumc_base_preprocess_menu_block_wrapper(&$variables, $hook) {
  // Add admin title to classes
  if (isset($variables['config']['admin_title']) && !empty($variables['config']['admin_title'])) {
    $variables['classes_array'][] = drupal_html_class($variables['config']['admin_title']);
  }
}

