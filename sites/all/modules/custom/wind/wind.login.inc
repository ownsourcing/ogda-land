<?php

/**
 * This function is used for logins that use the WIND Login form provided by
 * Columbia.
 */
function wind_wind_login() {
    $service = variable_get('wind_service', 'cumc-modauth');
    $login_uri = variable_get("wind|wind_service|login_url");
    $drupal_validate_uri = url('user/login/wind/validate', array('absolute' => TRUE));
    $login_link = "$login_uri?service=$service&destination=$drupal_validate_uri";
    drupal_goto($login_link);
}

/**
 * Validates the information recieved from the WIND login form, by ensuring the ticket
 * recieved is valid and looking up the user and attempting to login.
 */
function wind_wind_login_validate() {
  $query = drupal_get_query_parameters();
  $ticket_id = $query['ticketid'];
  wind_login($ticket_id); 
}

function wind_drupal_login($form, &$form_state) {
  global $base_url;
  $values = $form_state['values'];
  
  $username = $values['name'];
  $password = $values['pass'];

  $ticket_id = wind_authentication_native_authenticate($username, $password);
  wind_login($ticket_id);
}

/**
 * If the ticketid is provided in the query string, returns the ticketid.
 * This is a utility page so it's easy to get the ticketid when logging in via WIND with
 * the native Drupal login
 */
function wind_auth_login_ticket() {
  $query = drupal_get_query_parameters();
  
  if(array_key_exists('ticketid', $query)) {
    print $query['ticketid'];
  } else {
    return '';
  }
}

function wind_validate_ticket($ticket_id) {
  $validate_uri = variable_get("wind|wind_service|validate_url");
  $wind_request = drupal_http_request("$validate_uri?ticketid=$ticket_id");
	$wind_response = $wind_request->data;
  
  $lines = preg_split("/[\s]/", $wind_response, 3);
  if ($lines[0] == "yes") {
    $uni = $lines[1];
    $affiliations = preg_split("/\s+/", $lines[2], -1, PREG_SPLIT_NO_EMPTY);
    return array('uni' => $uni, 'affiliations' => $affiliations);
  } else {
    return FALSE;
  }
}

function wind_user_authenticate($uni) {
  $autocreate = variable_get('wind_login_autocreate', 0);
  $user_name_suffix = variable_get('wind|user_information|user_name_suffix');
  $uni .= $user_name_suffix;

  if($account = user_external_load($uni)) {  
    user_external_login_register($uni, 'wind');   
    return $account;
  } else {
    if($autocreate) {
      user_external_login_register($uni, 'wind');
      return user_external_load($uni);
    }
  }
   return FALSE; 
}

function wind_login($ticket_id) {
  if($user_information = wind_validate_ticket($ticket_id)) {
      if($authenticated = wind_user_authenticate($user_information['uni'])) {
        drupal_set_message(t('You are logged in as <strong>@username</strong>.', array('@username' => $authenticated->name)));
        return drupal_goto('user');
      }
    }
  $message = variable_get('wind|messages|not_authorized', t("You are not authorized to access this site.<br>If you believe you've gotten this message in error, please contact the site administrator"));
  drupal_set_message($message, 'error');
  return drupal_goto(); 
}