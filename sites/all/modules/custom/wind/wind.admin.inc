<?php
/**
 * @file
 * Contains the administrative forms for WIND integration
 */

/**
 * Create a WIND User
 */
function wind_create_wind_user($form, &$form_state) {

  // Account information.
  $form['account'] = array(
    '#type' => 'container',
    '#weight' => -10,
  );
  // Only show name field on registration form or user can change own username.
  $form['account']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username (UNI)'),
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
    '#required' => TRUE,
    '#attributes' => array('class' => array('username')),
    '#weight' => -10,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add UNI Account'),
  );
  return $form;
}

function wind_create_wind_user_validate($form, &$form_state) {

}

function wind_create_wind_user_submit($form, &$form_state) {
  global $user;
  $current_user = $user;
  $values = $form_state['values'];
  $user_name_suffix = variable_get('wind|user_information|user_name_suffix');
  $username = $values['name'] . $user_name_suffix;

  user_external_login_register($username, 'wind');
  $user = user_load($current_user->uid);
  drupal_set_message(t('Added UNI Account @account', array('@account' => $username)));
  drupal_goto('admin/people');
}

/**
 * Admin settings for WIND
 */
/**
 *  Function creates the admin configuration form
 *
 *  *removed many settings (but not all) from original module that were not applicable for HSL*
 */

function wind_admin_settings() {
  $form['wind_settings_title'] = array(
    '#type' => 'item',
    '#title' => t('WIND Integration Settings'),
  );
  $form['wind'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['wind']['login_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['wind']['login_type']['wind_external_wind_login_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add external WIND Login'),
    '#description' => t('This provides a button on the user login form that will take a user to the Columbia WIND Login page to authenticate.'),
    '#default_value' => variable_get('wind_external_wind_login_type', 0),
    '#required' => FALSE,
    '#group' => 'wind',
  );
  $form['wind']['login_type']['wind_external_wind_login_type_weight'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Add external WIND Login Weight'),
    '#description' => t('The weight controls where the link will appear on the forms'),
    '#default_value' => variable_get('wind_external_wind_login_type_weight', -1),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="wind_external_wind_login_type"]' => array('checked' => TRUE)
      ),
    ), 
  );  
  $form['wind']['login_type']['wind_secondary_wind_login_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Secondary Button for WIND Login'),
    '#description' => t('This will add a secondary button to the user login block that will use custom validation via Columbia WIND.'),
    '#default_value' => variable_get('wind_secondary_wind_login_type', 0),
    '#required' => FALSE,
    '#group' => 'wind',
  );
  $form['wind']['login_type']['wind_secondary_wind_login_type_weight'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Add external WIND Login Weight'),
    '#description' => t('The weight controls where the link will appear on the forms'),
    '#default_value' => variable_get('wind_secondary_wind_login_type_weight', 0),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="wind_secondary_wind_login_type"]' => array('checked' => TRUE)
      ),
    ), 
  );    
  $form['wind']['login_type']['wind_native_wind_login_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Drupal Native login for WIND Login'),
    '#description' => t('This will alter the Drupal login to first attempt to log a user in via WIND, if that fails it will attempt to log a user in via Drupal\'s local user authentication.'),
    '#default_value' => variable_get('wind_native_wind_login_type', 0),
    '#required' => FALSE,
    '#group' => 'wind',
  );  
  $form['wind']['wind_service'] = array(
    '#type' => 'fieldset',
    '#title' => t('WIND Service'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['wind']['wind_service']['wind|wind_service|login_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Login URL'),
    '#default_value' => variable_get('wind|wind_service|login_url'),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('The URL for a user to login to the WIND service.'),
  );

  $form['wind']['wind_service']['wind|wind_service|logout_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Logout URL'),
    '#default_value' => variable_get('wind|wind_service|logout_url'),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('The URL for a user to logout of the WIND service.'),
  );

  $form['wind']['wind_service']['wind|wind_service|validate_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Validate URL'),
    '#default_value' => variable_get('wind|wind_service|validate_url'),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('The URL to validate a user at the WIND service.'),
  );

   $form['wind']['messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Messages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['wind']['messages']['wind|messages|not_authorized'] = array(
    '#type' => 'textarea',
    '#title' => t('Not Authorized'),
    '#rows' => 5,
    '#cols' => 64,
    '#default_value' => variable_get('wind|messages|not_authorized',t("You are not authorized to access this site.<br>If you believe you've gotten this message in error, please contact the site administrator")),
    '#description' => t('Message for users who are not authorized to login to this system.'),
    '#required' => TRUE,
  );

  $form['wind']['user_information'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['wind']['user_information']['wind|user_information|user_name_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Suffix'),
    '#default_value' => variable_get('wind|user_information|user_name_suffix'),
    '#description' => t('Append this suffix to all created users. This can help separate user accounts automatically created through WIND vs. native drupal accounts. For example, you might set this to "@columbia.edu" so all user names are their e-mail addresses.'),
    '#size' => 60,
    '#maxlength' => 100,
  );
  $form['wind']['user_information']['wind_request_new_password'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Request New Password'),
    '#description' => t('Disables the request new password link in the User Login Block'),
    '#default_value' => variable_get('wind_request_new_password', 0),
    '#required' => FALSE,
    '#group' => 'wind',
  );  

  $form['wind']['wind_email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default E-mail Domain'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['wind']['wind_email']['wind_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Default E-mail Domain'),
    '#default_value' => variable_get('wind_email', 'columbia.edu'),
    '#description' => t('This will set the email address for the user automatically, when the account is created. Do NOT include the "@" sign, so e.g. "columbia.edu".'),
    '#size' => 60,
    '#maxlength' => 100,
    '#required' => TRUE,
    '#group' => 'wind',
  );

  $form['wind']['wind_service'] = array(
    '#type' => 'fieldset',
    '#title' => t('WIND service realm'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['wind']['wind_service']['wind_service'] = array(
    '#type' => 'textfield',
    '#title' => t('WIND service realm'),
    '#default_value' => variable_get('wind_service', 'cumc-modauth'),
    '#description' => t('The service realm for this website. This is assigned by the WIND group. Ex. "law-web".'),
    '#size' => 60,
    '#maxlength' => 100,
    '#required' => TRUE,
    '#group' => 'wind',
  );
  $form['wind']['wind_login_autocreate'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account Auto-creation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['wind']['wind_login_autocreate']['wind_login_autocreate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-create user accounts'),
    '#description' => t('Auto-create accounts when a user attempts to login with WIND but does not yet have an account.'),
    '#default_value' => variable_get('wind_login_autocreate', 0),
    '#required' => FALSE,
    '#group' => 'wind',
  );
  return system_settings_form($form);
}

