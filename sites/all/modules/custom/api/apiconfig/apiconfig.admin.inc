<?php

function apiconfig_configuration($form, &$form_state) {
  $form['apiconfig_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name or Department Name'),
    '#default_value' => variable_get('apiconfig_site_name', ''),
    '#description' => t('Provide site name or department name from the drop down below.'),
  );
  $form['apiconfig_department_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Department Id'),
    '#default_value' => variable_get('apiconfig_department_id', '0'),
    '#element_validate' => array('element_validate_integer'),
    '#description' => t('CUPS department id or enter 0 for all departments.'),
  );
  $form['apiconfig_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Key'),
    '#default_value' => variable_get('apiconfig_site_key', ''),
    '#description' => t('Provided site key by CUMC WebServices team.'),
    '#required' => TRUE,
  );
  $form['apiconfig_site_public_or_private'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is this public or private site'),
    '#default_value' => variable_get('apiconfig_site_public_or_private', 0),
    '#description' => t('Check this checkbox if you do not want public to access CUPS elements.'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}

?>