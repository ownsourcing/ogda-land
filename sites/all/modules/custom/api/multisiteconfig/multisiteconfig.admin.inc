<?php

function multisiteconfig_configuration($form, &$form_state) {
  $form['nursing_site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nursing Site Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  //NURSING SITE SETTINGS
  $form['nursing_site_settings']['nursing_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#default_value' => variable_get('nursing_site_name', 'Nursing'),
    '#disabled' => TRUE,
  );
  $form['nursing_site_settings']['nursing_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Id'),
    '#default_value' => variable_get('nursing_site_id', '11'),
    '#disabled' => TRUE,
  );	
  $form['nursing_site_settings']['nursing_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Key'),
    '#default_value' => variable_get('nursing_site_key', 'NURSING'),
    '#description' => t('Provided site key by CUMC WebServices team.'),
    '#required' => TRUE,
  );
  $form['nursing_site_settings']['nursing_site_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Folder'),
    '#default_value' => variable_get('nursing_site_folder', 'school-nursing'),
    '#description' => t('Provided site folder.'),
    '#required' => TRUE,
  );

  //EYE/OPTHALMOLOGY SITE SETTINGS
  $form['opthalmology_site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Opthalmology Site Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['opthalmology_site_settings']['opthalmology_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#default_value' => variable_get('opthalmology_site_name', 'Opthalmology'),
    '#disabled' => TRUE,
  );
  $form['opthalmology_site_settings']['opthalmology_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Id'),
    '#default_value' => variable_get('opthalmology_site_id', '3'),
    '#disabled' => TRUE,
  );
  $form['opthalmology_site_settings']['opthalmology_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Key'),
    '#default_value' => variable_get('opthalmology_site_key', 'COLUMBIAEYE'),
    '#description' => t('Provided site key by CUMC WebServices team.'),
    '#required' => TRUE,
  );
  $form['opthalmology_site_settings']['opthalmology_site_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Folder'),
    '#default_value' => variable_get('opthalmology_site_folder', 'opthalmology'),
    '#description' => t('Provided site folder.'),
    '#required' => TRUE,
  );

  //ADRC SITE SETTINGS
  $form['adrc_site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('ADRC Site Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['adrc_site_settings']['adrc_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#default_value' => variable_get('adrc_site_name', 'ADRC'),
    '#disabled' => TRUE,
  );
  $form['adrc_site_settings']['adrc_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Id'),
    '#default_value' => variable_get('adrc_site_id', '19'),
    '#disabled' => TRUE,
  );
  $form['adrc_site_settings']['adrc_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Key'),
    '#default_value' => variable_get('adrc_site_key', 'ADRC'),
    '#description' => t('Provided site key by CUMC WebServices team.'),
    '#required' => TRUE,
  );
  $form['adrc_site_settings']['adrc_site_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Folder'),
    '#default_value' => variable_get('adrc_site_folder', 'adrc'),
    '#description' => t('Provided site folder.'),
    '#required' => TRUE,
  );

  //NEUROLOGY SITE SETTINGS
  $form['neurology_site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('NEUROLOGY Site Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['neurology_site_settings']['neurology_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#default_value' => variable_get('neurology_site_name', 'NEUROLOGY'),
    '#disabled' => TRUE,
  );
  $form['neurology_site_settings']['neurology_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Id'),
    '#default_value' => variable_get('neurology_site_id', '6'),
    '#disabled' => TRUE,
  );
  $form['neurology_site_settings']['neurology_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Key'),
    '#default_value' => variable_get('neurology_site_key', 'NEUROLOGY'),
    '#description' => t('Provided site key by CUMC WebServices team.'),
    '#required' => TRUE,
  );
  $form['neurology_site_settings']['neurology_site_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Folder'),
    '#default_value' => variable_get('neurology_site_folder', 'neurology'),
    '#description' => t('Provided site folder.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

?>