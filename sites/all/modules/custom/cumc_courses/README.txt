INTRODUCTION
------------
Provides default Views view CUMC Courses Exporter to generate CSV file of
courses (node type nursing_course_list), and a default Feeds importer CUMC
Courses Importer to reimport the CSV file.

CUMC Courses Importer utilises Feeds Tamper to explode Course Type and Tags
field values allowing import of multiple Taxonomy terms per course.

REQUIREMENTS
------------
CUMC Courses module is, by default, set to work with Nodes of type Nursing Course
List (nursing_course_list), and also requires the following contributed modules:
 * Feeds (https://drupal.org/project/feeds)
 * Feeds Tamper (https://drupal.org/project/feeds_tamper)
 * Views (https://drupal.org/project/views)
 * Views Data Export (https://drupal.org/project/views_data_export)

It is recommended to use Feeds Tamper UI to ensure multi-value taxonomy term
reference fields such as Course Type will be separated into individual terms on
import, see /admin/structure/feeds/cumc_courses_importer/tamper.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See
   https://drupal.org/documentation/install/modules-themes/modules-7 for further
   information.

CONFIGURATION
-------------
 * Configure CUMC Courses Exporter View in Administration >> Structure >> Views:
   - default Node Type is nursing_course_list.
   - default path for generating CSV is /course-list/export, adjust as required,
     see /admin/structure/views/view/cumc_courses_exporter/edit
 * Configure CUMC Courses Importer Feed in Administration >> Structure >> Feeds:
   - default Node Type is nursing_course_list.
   - check Tamper settings, see /admin/structure/feeds/cumc_courses_importer/tamper

TROUBLESHOOTING
---------------
 * If nodes are not imported when uploading an edited CSV:
   - set Skip Hash Check to TRUE to force update of all nodes, see
     /admin/structure/feeds/cumc_courses_importer/settings/FeedsNodeProcessor

MAINTAINERS
-----------
 * Lisa Walley (lwalley) - https://drupal.org/user/604534
