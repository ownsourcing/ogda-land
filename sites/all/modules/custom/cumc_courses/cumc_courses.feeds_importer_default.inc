<?php
/**
 * @file
 * cumc_courses.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function cumc_courses_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'cumc_courses_importer';
  $feeds_importer->config = array(
    'name' => 'CUMC Courses Importer',
    'description' => 'Import course nodes from CSV.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => FALSE,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Course Number',
            'target' => 'field_course_number',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Course Name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Credits',
            'target' => 'field_credits',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Credits Comments',
            'target' => 'field_credits_comments',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Prerequisite',
            'target' => 'field_prerequisite',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Corequisite',
            'target' => 'field_corequisite',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Prereq and Coreq',
            'target' => 'field_prereq_and_coreq',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Description',
            'target' => 'field_description',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Specific Comment',
            'target' => 'field_specific_comment',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Link Number',
            'target' => 'field_link_number',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Spec Course ID',
            'target' => 'field_spec_course_id',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Specialty',
            'target' => 'field_specialty',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Term',
            'target' => 'field_term',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Updated',
            'target' => 'field_updated',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Type',
            'target' => 'field_type',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'Spec ID',
            'target' => 'field_spec_id',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'Notes',
            'target' => 'field_notes',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'Course Type',
            'target' => 'field_course_type',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'Tags',
            'target' => 'field_course_tags',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'Node ID',
            'target' => 'nid',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'filtered_html',
        'skip_hash_check' => 0,
        'bundle' => 'nursing_course_list',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['cumc_courses_importer'] = $feeds_importer;

  return $export;
}
