<?php
/**
 * @file
 * cumc_courses.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cumc_courses_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cumc_courses_exporter';
  $view->description = 'Export courses as CSV.';
  $view->tag = 'cumc';
  $view->base_table = 'node';
  $view->human_name = 'CUMC Courses Exporter';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Nursing Courses';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    11 => '11', /* Administrator */
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1000';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['sitename_title'] = 0;
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'body' => 'body',
    'field_corequisite' => 'field_corequisite',
    'field_course_number' => 'field_course_number',
    'field_course_type' => 'field_course_type',
    'delta' => 'delta',
    'field_credits' => 'field_credits',
    'field_credits_comments' => 'field_credits_comments',
    'field_description' => 'field_description',
    'field_link_number' => 'field_link_number',
    'field_notes' => 'field_notes',
    'field_prereq_and_coreq' => 'field_prereq_and_coreq',
    'field_prerequisite' => 'field_prerequisite',
    'field_spec_course_id' => 'field_spec_course_id',
    'field_spec_id' => 'field_spec_id',
    'field_specialty' => 'field_specialty',
    'field_specific_comment' => 'field_specific_comment',
    'field_course_tags' => 'field_course_tags',
    'field_term' => 'field_term',
    'field_type' => 'field_type',
    'field_updated' => 'field_updated',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_corequisite' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_course_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_course_type' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delta' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_credits' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_credits_comments' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_link_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_notes' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_prereq_and_coreq' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_prerequisite' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_spec_course_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_spec_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_specialty' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_specific_comment' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_course_tags' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_term' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_updated' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = FALSE;
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Field: Content: Course Number */
  $handler->display->display_options['fields']['field_course_number']['id'] = 'field_course_number';
  $handler->display->display_options['fields']['field_course_number']['table'] = 'field_data_field_course_number';
  $handler->display->display_options['fields']['field_course_number']['field'] = 'field_course_number';
  $handler->display->display_options['fields']['field_course_number']['element_type'] = '0';
  $handler->display->display_options['fields']['field_course_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_course_number']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_course_number']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_course_number']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_course_number']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Course Name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Credits */
  $handler->display->display_options['fields']['field_credits']['id'] = 'field_credits';
  $handler->display->display_options['fields']['field_credits']['table'] = 'field_data_field_credits';
  $handler->display->display_options['fields']['field_credits']['field'] = 'field_credits';
  $handler->display->display_options['fields']['field_credits']['element_type'] = '0';
  $handler->display->display_options['fields']['field_credits']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_credits']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_credits']['element_default_classes'] = FALSE;
  /* Field: Content: Credits Comments */
  $handler->display->display_options['fields']['field_credits_comments']['id'] = 'field_credits_comments';
  $handler->display->display_options['fields']['field_credits_comments']['table'] = 'field_data_field_credits_comments';
  $handler->display->display_options['fields']['field_credits_comments']['field'] = 'field_credits_comments';
  $handler->display->display_options['fields']['field_credits_comments']['element_type'] = '0';
  $handler->display->display_options['fields']['field_credits_comments']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_credits_comments']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_credits_comments']['element_default_classes'] = FALSE;
  /* Field: Content: Prerequisite */
  $handler->display->display_options['fields']['field_prerequisite']['id'] = 'field_prerequisite';
  $handler->display->display_options['fields']['field_prerequisite']['table'] = 'field_data_field_prerequisite';
  $handler->display->display_options['fields']['field_prerequisite']['field'] = 'field_prerequisite';
  $handler->display->display_options['fields']['field_prerequisite']['element_type'] = '0';
  $handler->display->display_options['fields']['field_prerequisite']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_prerequisite']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_prerequisite']['element_default_classes'] = FALSE;
  /* Field: Content: Corequisite */
  $handler->display->display_options['fields']['field_corequisite']['id'] = 'field_corequisite';
  $handler->display->display_options['fields']['field_corequisite']['table'] = 'field_data_field_corequisite';
  $handler->display->display_options['fields']['field_corequisite']['field'] = 'field_corequisite';
  $handler->display->display_options['fields']['field_corequisite']['element_type'] = '0';
  $handler->display->display_options['fields']['field_corequisite']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_corequisite']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_corequisite']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_corequisite']['type'] = 'text_plain';
  /* Field: Content: Prereq and Coreq */
  $handler->display->display_options['fields']['field_prereq_and_coreq']['id'] = 'field_prereq_and_coreq';
  $handler->display->display_options['fields']['field_prereq_and_coreq']['table'] = 'field_data_field_prereq_and_coreq';
  $handler->display->display_options['fields']['field_prereq_and_coreq']['field'] = 'field_prereq_and_coreq';
  $handler->display->display_options['fields']['field_prereq_and_coreq']['element_type'] = '0';
  $handler->display->display_options['fields']['field_prereq_and_coreq']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_prereq_and_coreq']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_prereq_and_coreq']['element_default_classes'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['element_type'] = '0';
  $handler->display->display_options['fields']['field_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_description']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_description']['element_default_classes'] = FALSE;
  /* Field: Content: Specific Comment */
  $handler->display->display_options['fields']['field_specific_comment']['id'] = 'field_specific_comment';
  $handler->display->display_options['fields']['field_specific_comment']['table'] = 'field_data_field_specific_comment';
  $handler->display->display_options['fields']['field_specific_comment']['field'] = 'field_specific_comment';
  $handler->display->display_options['fields']['field_specific_comment']['element_type'] = '0';
  $handler->display->display_options['fields']['field_specific_comment']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_specific_comment']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_specific_comment']['element_default_classes'] = FALSE;
  /* Field: Content: Link Number */
  $handler->display->display_options['fields']['field_link_number']['id'] = 'field_link_number';
  $handler->display->display_options['fields']['field_link_number']['table'] = 'field_data_field_link_number';
  $handler->display->display_options['fields']['field_link_number']['field'] = 'field_link_number';
  $handler->display->display_options['fields']['field_link_number']['element_type'] = '0';
  $handler->display->display_options['fields']['field_link_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link_number']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_link_number']['element_default_classes'] = FALSE;
  /* Field: Content: Spec Course ID */
  $handler->display->display_options['fields']['field_spec_course_id']['id'] = 'field_spec_course_id';
  $handler->display->display_options['fields']['field_spec_course_id']['table'] = 'field_data_field_spec_course_id';
  $handler->display->display_options['fields']['field_spec_course_id']['field'] = 'field_spec_course_id';
  $handler->display->display_options['fields']['field_spec_course_id']['element_type'] = '0';
  $handler->display->display_options['fields']['field_spec_course_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_spec_course_id']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_spec_course_id']['element_default_classes'] = FALSE;
  /* Field: Content: Specialty */
  $handler->display->display_options['fields']['field_specialty']['id'] = 'field_specialty';
  $handler->display->display_options['fields']['field_specialty']['table'] = 'field_data_field_specialty';
  $handler->display->display_options['fields']['field_specialty']['field'] = 'field_specialty';
  $handler->display->display_options['fields']['field_specialty']['element_type'] = '0';
  $handler->display->display_options['fields']['field_specialty']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_specialty']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_specialty']['element_default_classes'] = FALSE;
  /* Field: Content: Term */
  $handler->display->display_options['fields']['field_term']['id'] = 'field_term';
  $handler->display->display_options['fields']['field_term']['table'] = 'field_data_field_term';
  $handler->display->display_options['fields']['field_term']['field'] = 'field_term';
  $handler->display->display_options['fields']['field_term']['element_type'] = '0';
  $handler->display->display_options['fields']['field_term']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_term']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_term']['element_default_classes'] = FALSE;
  /* Field: Content: Updated */
  $handler->display->display_options['fields']['field_updated']['id'] = 'field_updated';
  $handler->display->display_options['fields']['field_updated']['table'] = 'field_data_field_updated';
  $handler->display->display_options['fields']['field_updated']['field'] = 'field_updated';
  $handler->display->display_options['fields']['field_updated']['element_type'] = '0';
  $handler->display->display_options['fields']['field_updated']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_updated']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_updated']['element_default_classes'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_type']['id'] = 'field_type';
  $handler->display->display_options['fields']['field_type']['table'] = 'field_data_field_type';
  $handler->display->display_options['fields']['field_type']['field'] = 'field_type';
  $handler->display->display_options['fields']['field_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_type']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_type']['element_default_classes'] = FALSE;
  /* Field: Content: Spec ID */
  $handler->display->display_options['fields']['field_spec_id']['id'] = 'field_spec_id';
  $handler->display->display_options['fields']['field_spec_id']['table'] = 'field_data_field_spec_id';
  $handler->display->display_options['fields']['field_spec_id']['field'] = 'field_spec_id';
  $handler->display->display_options['fields']['field_spec_id']['element_type'] = '0';
  $handler->display->display_options['fields']['field_spec_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_spec_id']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_spec_id']['element_default_classes'] = FALSE;
  /* Field: Content: Notes */
  $handler->display->display_options['fields']['field_notes']['id'] = 'field_notes';
  $handler->display->display_options['fields']['field_notes']['table'] = 'field_data_field_notes';
  $handler->display->display_options['fields']['field_notes']['field'] = 'field_notes';
  $handler->display->display_options['fields']['field_notes']['element_type'] = '0';
  $handler->display->display_options['fields']['field_notes']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_notes']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_notes']['element_default_classes'] = FALSE;
  /* Field: Content: Course Type */
  $handler->display->display_options['fields']['field_course_type']['id'] = 'field_course_type';
  $handler->display->display_options['fields']['field_course_type']['table'] = 'field_data_field_course_type';
  $handler->display->display_options['fields']['field_course_type']['field'] = 'field_course_type';
  $handler->display->display_options['fields']['field_course_type']['element_type'] = '0';
  $handler->display->display_options['fields']['field_course_type']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_course_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_course_type']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_course_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_course_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_course_type']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_course_type']['separator'] = ';';
  /* Field: Content: Tags */
  $handler->display->display_options['fields']['field_course_tags']['id'] = 'field_course_tags';
  $handler->display->display_options['fields']['field_course_tags']['table'] = 'field_data_field_course_tags';
  $handler->display->display_options['fields']['field_course_tags']['field'] = 'field_course_tags';
  $handler->display->display_options['fields']['field_course_tags']['element_type'] = '0';
  $handler->display->display_options['fields']['field_course_tags']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_course_tags']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_course_tags']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_course_tags']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_course_tags']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_course_tags']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_course_tags']['separator'] = ';';
  /* Field: Content: Published */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'node';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['status']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['status']['type'] = 'boolean';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'Node ID';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Course Number (field_course_number) */
  $handler->display->display_options['sorts']['field_course_number_value']['id'] = 'field_course_number_value';
  $handler->display->display_options['sorts']['field_course_number_value']['table'] = 'field_data_field_course_number';
  $handler->display->display_options['sorts']['field_course_number_value']['field'] = 'field_course_number_value';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'nursing_course_list' => 'nursing_course_list',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Nursing Courses */
  $handler = $view->new_display('views_data_export', 'Nursing Courses', 'views_data_export_nursing_courses');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'course-list/export';
  $export['cumc_courses_exporter'] = $view;

  return $export;
}
