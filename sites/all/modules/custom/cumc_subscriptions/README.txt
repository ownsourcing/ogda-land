INTRODUCTION
------------
Provides default customisations to the subscriptions suite of modules.

REQUIREMENTS
------------
 * Subscriptions (https://drupal.org/project/subscriptions)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See
   https://drupal.org/documentation/install/modules-themes/modules-7 for further
   information.

CONFIGURATION
-------------
None.

MAINTAINERS
-----------
 * Lisa Walley (lwalley) - https://drupal.org/user/604534
