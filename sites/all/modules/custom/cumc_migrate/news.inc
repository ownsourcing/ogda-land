<?php

abstract class CumcBaseMigration extends DynamicMigration {
  public function __construct() {
    parent::__construct();
    $this->team = array(
      new MigrateTeamMember('Benjamin Melançon', 'ben@agaric.com', t('Developer')),
    );
  }
}


/**
 * News migration.
 */
class NursingNewsMigration extends CumcBaseMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Migrate to Main Article nodes (creating Spread nodes for each) from nursing news data comma separated values file.');

    // Even from a CSV file, the mapping for the source looks like a schema definition.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'newsID' => array('type' => 'int',
                           'length' => 5,
                           'not null' => TRUE,
                           'description' => 'Unique integer ID',
                          ),
        ),
        MigrateDestinationNode::getKeySchema()
      );

    $columns = array(
      0 => array('newsID', 'Unique integer ID'),
      1 => array('title', 'Title'),
      2 => array('teaserPara', 'Teaser'),
      3 => array('content', 'Body'),
      6 => array('datePosted', 'Created')
    );
    $options = array('header_rows' => 1, 'embedded_newlines' => TRUE, 'delimiter' => ',', 'enclosure' => '"');
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'cumc_migrate') . '/data/nursing_news_data3_prepped.csv', $columns, $options);

    $this->destination = new MigrateDestinationNode('main_article', array('text_format' => 'full_html'));

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body:summary', 'teaserPara');
    $this->addFieldMapping('body', 'content');
    $this->addFieldMapping('created', 'datePosted')
         ->description('Figures out the correct date by magic.');

    // Defaults
    $this->addFieldMapping('uid')->defaultValue(11)->description('Assign authorship to Grace.');
    $this->addFieldMapping('field_type_of_content')->defaultValue('news')->description('News sub-type.');
    $this->addFieldMapping('field_can_be_shared')->defaultValue(TRUE)->description('Article can be shared.');

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }

  function prepareRow($row) {
    $row->teaserPara = _filter_autop($row->teaserPara);
    $row->content = _filter_autop($row->content);
  }
}

/**
 * Spreads for news migration.
 */
class NursingNewsSpreadMigration extends CumcBaseMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Create Spread nodes for each nursing news item previously imported.');

    $this->dependencies = array('NursingNews');

    // Even from a CSV file, the mapping for the source looks like a schema definition.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'newsID' => array('type' => 'int',
                           'length' => 5,
                           'not null' => TRUE,
                           'description' => 'Unique integer ID',
                          ),
        ),
        MigrateDestinationNode::getKeySchema()
      );

    $columns = array(
      0 => array('newsID', 'Unique integer ID'),
      6 => array('datePosted', 'Created')
    );
    $options = array('header_rows' => 1, 'embedded_newlines' => TRUE, 'delimiter' => ',', 'enclosure' => '"');
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'cumc_migrate') . '/data/nursing_news_data3_prepped.csv', $columns, $options);

    $this->destination = new MigrateDestinationNode('spread', array('text_format' => 'full_html'));

    $this->addFieldMapping('field_referenced_article', 'newsID')
      ->sourceMigration('NursingNews');

    // Defaults
    $this->addFieldMapping('uid')->defaultValue(11)->description('Assign authorship to Grace.');
    $this->addFieldMapping('field_category')->defaultValue('Health')->description('Health category.');
    $this->addFieldMapping('og_group_ref')->defaultValue(246)->description('School of Nursing.');

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }
}

/**
 * News Archive migration.
 */
class NursingNewsArchiveMigration extends CumcBaseMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Migrate to Main Article nodes (creating Spread nodes for each) from nursing news data comma separated values file.');

    // Even from a CSV file, the mapping for the source looks like a schema definition.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'newsID' => array('type' => 'int',
                           'length' => 5,
                           'not null' => TRUE,
                           'description' => 'Unique integer ID',
                          ),
        ),
        MigrateDestinationNode::getKeySchema()
      );

    $columns = array(
      0 => array('newsID', 'Unique integer ID'),
      1 => array('title', 'Title'),
      3 => array('content', 'Body'),
      6 => array('datePosted', 'Created')
    );
    $options = array('header_rows' => 1, 'embedded_newlines' => TRUE, 'delimiter' => ',', 'enclosure' => '"');
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'cumc_migrate') . '/data/nursing_news_archives_data_merge3_prepped.csv', $columns, $options);

    $this->destination = new MigrateDestinationNode('main_article', array('text_format' => 'full_html'));

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'content');
    $this->addFieldMapping('created', 'datePosted')
         ->description('Figures out the correct date by magic.');

    // Defaults
    $this->addFieldMapping('uid')->defaultValue(11)->description('Assign authorship to Grace.');
    $this->addFieldMapping('field_type_of_content')->defaultValue('news')->description('News sub-type.');
    $this->addFieldMapping('field_can_be_shared')->defaultValue(TRUE)->description('Article can be shared.');

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }

  function prepareRow($row) {
    $row->content = _filter_autop($row->content);
  }
}

/**
 * Spreads for news migration.
 */
class NursingNewsArchiveSpreadMigration extends CumcBaseMigration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Create Spread nodes for each nursing news archive item previously imported.');

    $this->dependencies = array('NursingNewsArchive');

    // Even from a CSV file, the mapping for the source looks like a schema definition.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'newsID' => array('type' => 'int',
                           'length' => 5,
                           'not null' => TRUE,
                           'description' => 'Unique integer ID',
                          ),
        ),
        MigrateDestinationNode::getKeySchema()
      );

    $columns = array(
      0 => array('newsID', 'Unique integer ID'),
      6 => array('datePosted', 'Created')
    );
    $options = array('header_rows' => 1, 'embedded_newlines' => TRUE, 'delimiter' => ',', 'enclosure' => '"');
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'cumc_migrate') . '/data/nursing_news_archives_data_merge3_prepped.csv', $columns, $options);

    $this->destination = new MigrateDestinationNode('spread', array('text_format' => 'full_html'));

    $this->addFieldMapping('field_referenced_article', 'newsID')
      ->sourceMigration('NursingNewsArchive');

    // Defaults
    $this->addFieldMapping('uid')->defaultValue(11)->description('Assign authorship to Grace.');
    $this->addFieldMapping('field_category')->defaultValue('Health')->description('Health category.');
    $this->addFieldMapping('og_group_ref')->defaultValue(246)->description('School of Nursing.');

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and or pathauto enabled
    if (module_exists('path')) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }
}
