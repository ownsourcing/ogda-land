<?php

function cups_addresses_configuration($form, &$form_state) {
  $form['cups_addresses_class_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Class Name of address field.'),
    '#default_value' => variable_get('cups_addresses_class_name', ''),
    '#description' => t('Provide class name where you want to apply CUPS address autosuggest.'),
  );
  return system_settings_form($form);
}

?>