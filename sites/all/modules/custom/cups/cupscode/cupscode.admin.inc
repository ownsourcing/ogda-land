<?php

function cupscode_settings() {
	//--------------------------------------------------------------
	$form['cups_subdomain_link'] = array(
		'#type' => 'textfield',
		'#title' => t('Sub Domain Link'),
		'#description' => t('Indicate folder name of this site on the server if this site is not using a vanity url. Leave it blank if you have dedicated domain for this site.'),
		'#default_value' => variable_get('cups_subdomain_link'),
		'#required' => FALSE,
	);
	$form['cups_settings_path'] = array(
		'#type' => 'textfield',
		'#title' => t('CUPS Settings Path'),
		'#description' => t('Indicate path to CUPS settings folder on this server. Leave it blank if this site is not using CUPS settings folder.'),
		'#default_value' => variable_get('cups_settings_path'),
		'#required' => FALSE,
	);
	//--------------------------------------------------------------
	return system_settings_form($form);
}
