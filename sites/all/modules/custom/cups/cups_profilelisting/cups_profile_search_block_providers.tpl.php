<?php 
	$subDomainLink = ''; 
	$LinkVar = variable_get('cups_subdomain_link');

	if(isset($LinkVar) && $LinkVar != ''){
		$subDomainLink = '/' . $LinkVar;
	}
	global $base_url;
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script>

	function UpdateOptions(itemClassName){

		$('.find-a-doc')[0].reset();
		$('.optionsdiv').show();
		$('.liactiveclass').removeClass("active");
		$(itemClassName+'_li').addClass("active");
		$('.searchoption').hide();
		$(itemClassName).show();

	}

	function GenerateSelectOptions(selectid, selecttype, selectedid)
	{
		$(selectid).append('<option value="0">Select One</option>');
		$.ajax({
		url: "http://columbiaprofiles.com/api/apisites/" + selecttype,
		dataType: "jsonp",
		data: {
			method: "GET",
			SiteId: "<?php print variable_get('apiconfig_department_id'); ?>",
			SiteKey: "<?php print variable_get('apiconfig_site_key'); ?>",
			ProfileType: '<?php print $profileActionType; ?>'
		},
		success: function( data ) {
			$(data.OptionsList).each(function(){
				var optName = $(this).attr("SelectName");
				var optId = $(this).attr("SelectId");
				var selectOption = '<option value="'+optId+'">'+optName +'</option>';
				
				if(optId == selectedid){
					selectOption = '<option selected="selected" value="'+optId+'">'+optName+'</option>';
				}
				$(selectid).append(selectOption);	
			
			});
			}
		});	
	}

	function PopulateDptDivision(id)
	{
		var disableDivision = true;
		$('.dptdivisions').empty().append('<option value="0">Select Division</option>');
		var dptId = id;
		$.ajax({
		url: "http://columbiaprofiles.com/api/apisites/getDepartmentDivisions",
		dataType: "jsonp",
		data: {
			method: "GET",
			SiteId: "<?php print variable_get('apiconfig_department_id'); ?>",
			SiteKey: "<?php print variable_get('apiconfig_site_key'); ?>",
			DepartmentId: dptId,
			ProfileType: '<?php print $profileActionType; ?>'
		},
		success: function( data ) {
			$(data.OptionsList).each(function(){
				var divName = $(this).attr("SelectName");
				var divId = $(this).attr("SelectId");
			
				var divOption = '<option value="'+divId+'">'+divName+'</option>';
				if(divId == "<?php print $Division; ?>"){
					divOption = '<option selected="selected" value="'+divId+'">'+divName+'</option>';
				}
				$('.dptdivisions').append(divOption);
				disableDivision = false;
			});
			if(disableDivision){
			$('.dptdivisions').hide();
			}
			else{
			$('.dptdivisions').show();
			}
			}
		});
		
	}
</script>

<?php if($variables['profileListType'] == 'provider/search.php') : ?>

<script>
$(function() {
$('.LastName').autocomplete({
	source: function( request, response ) {
		$.ajax({
		url: "http://columbiaprofiles.com/api/apisites/getAutoSuggestLastNamesProviders",
		dataType: "jsonp",
		data: {
			LastNameTerm: request.term,
			method: "GET",
			SiteId: "<?php print variable_get('apiconfig_department_id'); ?>",
			SiteKey: "<?php print variable_get('apiconfig_site_key'); ?>"
		},
		success: function( data ) {
			response( $.map( data.Terms, function( item ) {
				return {
					label: item,
					value: item
				}
			}));
			}
		});
	},
	minLength: 2
});
});
</script>

<script>
$(function() {
$('.SpecName').autocomplete({
	source: function( request, response ) {
		$.ajax({
		url: "http://columbiaprofiles.com/api/apisites/getAutoSuggestSpecNamesProviders",
		dataType: "jsonp",
		data: {
			featureClass: "P",
			style: "full",
			maxRows: 12,
			SpecNameTerm: request.term,
			method: "GET",
			SiteId: "<?php print variable_get('apiconfig_department_id'); ?>",
			SiteKey: "<?php print variable_get('apiconfig_site_key'); ?>"
		},
		success: function( data ) {
			response( $.map( data.Terms, function( item ) {
				return {
					label: item,
					value: item
				}
			}));
			}
		});
	},
	minLength: 2
});
});
</script>

<?php endif; ?>

<script>
	$(document).ready(function () {

		$('.byname').show();

		$('.byname_li').addClass("active");

		UpdateOptions('.byname');

		<?php if($variables['SelectSearchType'] == 'LastName' && $variables['LastName'] != '') : ?>
			$('.bylastname').show();
			$('.byspecname').hide();
		<?php endif; ?>

		<?php if($variables['SelectSearchType'] == 'Specialty' && $variables['Specialty'] != '') : ?>
			$('.byspecname').show();
			$('.bylastname').hide();
		<?php endif; ?>

		$('.departments').change(function() {
			var dptId = $(this).val();
			if(dptId > 0)
			{
				PopulateDptDivision(dptId);
			}
		});
		
		<?php if($moreoptions) : ?>

			GenerateSelectOptions('.departments', 'getAllDepartments', '<?php print $Department; ?>');
			GenerateSelectOptions('.insurances', 'getAllInsurances', '<?php print $Insurance; ?>');
			GenerateSelectOptions('.languages', 'getAllLanguages', '<?php print $Language; ?>');
			GenerateSelectOptions('.locations', 'getAllLocations', '<?php print $Location; ?>');

			$('.optionsdiv').show();

		<?php endif; ?>

		<?php if($isRefine) : ?>
			$('.searchoption').show();
		<?php endif; ?>

		<?php if($Department > 0) { ?>
			PopulateDptDivision('<?php print $Department; ?>');
		<?php }else{ ?>
			$('.dptdivisions').hide();
		<?php } ?>
	});

</script>

<form class="find-a-doc" action="<?php print $subDomainLink; ?>/<?php print $profileListType; ?>">
	<?php if(!$isRefine) : ?>
	<ul class="searchlabels">
	<?php if($searchbyname) : ?>
		<li style="cursor: pointer;" class="byname_li liactiveclass" onclick="UpdateOptions('.byname')">Search By Name</li>
	<?php endif; ?>
	<?php if($searchbyspecialty) : ?>
		<li style="cursor: pointer;" class="byspecialty_li liactiveclass" onclick="UpdateOptions('.byspecialty'); $('.SpecName').show();">Search By Specialty</li>
	<?php endif; ?>
	<?php if($searchbydepartment) : ?>
		<li style="cursor: pointer;" class="bydepartmentdivisison_li liactiveclass" onclick="UpdateOptions('.bydepartmentdivisison'); $('.dptdivisions').hide();">Search By Department</li>
	<?php endif; ?>
	<?php if($searchbyresearchinterests) : ?>
		<li style="cursor: pointer;" class="byresearchinterests_li liactiveclass" onclick="UpdateOptions('.ResearchInterest'); $('.byresearchinterests').show();">Search By Research Interest</li>
	<?php endif; ?>
	<?php if($listall) : ?>
		<li style="cursor: pointer;"><a href="<?php print $subDomainLink; ?>/<?php print $profileActionType; ?>/showall">List All</a></li>
	<?php endif; ?>
	<?php if($moreoptions) : ?>
		<li style="cursor: pointer;" class="searchoption_li liactiveclass" onclick="UpdateOptions('.searchoption')"><a href="#" class="moreoptionslink">More Options</a></li>
	<?php endif; ?>
	</ul>
	<?php endif; ?>

	<div class="optionsdiv" style="display:none;">
		<?php if($searchbyispeds) : ?>
			<div class="pedssearchtoggle searchoption">
				<span class="label">limit your search to pediatric providers</span>
				<input type="checkbox" name="ispediatrics" class="ispediatrics" <?php if($variables['IsPeds'] == 'on') :?> checked="checked" <?php endif; ?>> 
			</div>
			<br/>
		<?php endif; ?>
			
		<?php if($searchbyname) : ?>
			<div class="byname searchoption">
				<?php if($isRefine) : ?>Last Name<?php endif; ?>
				<input type="text" class="LastName" name="LastName" placeholder="Last Name" value="<?php print $variables['LastName']; ?>"/>
			</div>
		<?php endif; ?>

		<?php if($searchbyspecialty) : ?>
			<div class="byspecialty searchoption">
				<?php if($isRefine) : ?>Specialty<?php endif; ?>
				<input type="text" class="SpecName" name="Specialty" placeholder="Specialty Name" value="<?php print $variables['Specialty']; ?>"/>
			</div>
		<?php endif; ?>

		<?php if(isset($searchbydepartment) && $searchbydepartment) : ?>
			<div class="bydepartmentdivisison searchoption">
				Department <select name="Department" class="departments"></select>
				<select name="Division" class="dptdivisions"></select>
			</div>
			<?php endif; ?>

		<?php if(isset($searchbyinsurance) && $searchbyinsurance) : ?>
			<div class="byinsurance searchoption">
				Accepted Insurance <select name="Insurance" class="insurances"></select>
			</div>
		<?php endif; ?>

		
			<div class="bylanguage searchoption">
				Languages Spoken <select name="Language" class="languages"></select>
			</div>
	
		<?php if(isset($searchbygender) && $searchbygender) : ?>
			<div class="bygender searchoption">
				Gender <select name="Gender" id="genders">
					<option value="0">Select One</option>
					<option <?php if($Gender == 'Male' && $Gender != '0') : ?> selected="selected" <? endif; ?> value="Male">Male</option>
					<option <?php if($Gender == 'Female' && $Gender != '0') : ?> selected="selected" <? endif; ?> value="Female">Female</option>
				</select>
			</div>
		<?php endif; ?>

		<?php if(isset($searchbylocation) && $searchbylocation) : ?>
			<div class="bylocation searchoption">
				Location <select name="Location" class="locations"></select>
			</div>
		<?php endif; ?>

		<?php if(isset($searchbyzip) && $searchbyzip) : ?>
			<div class="byzip searchoption">
				<input type="text" class="ZipCode" name="ZipCode" placeholder="Zip Code" value="<?php print $variables['ZipCode']; ?>"/>
			</div>
		<?php endif; ?>

		<?php if($searchbyresearchinterests) : ?>
			<div class="byresearchinterests searchoption">
				Research Interest
				<input type="text" class="ResearchInterest" name="ResearchInterest" placeholder="Research Interest" value="<?php print $variables['ResearchInterest']; ?>"/>
			</div>
		<?php endif; ?>

	</div>
	<input type="submit" name="submit" value="Search" class="search" />
</form>