<?php
	$url = 'http://columbiaprofiles.com/api/apisites/GetAllResearchInterests';
	$urlParams = array(
		'method' => 'GET',
		'SiteId' => variable_get('apiconfig_department_id'),
		'SiteKey' => variable_get('apiconfig_site_key'),
	);

	$full_url = url($url, array('query' => $urlParams));
	$results = drupal_http_request($full_url);

	//print $full_url;

	if(isset($results)){
		print '<ul>';
		$resultsArray = json_decode($results->data, true);
		if(isset($resultsArray)){
			if($resultsArray['Count'] > 0){
				foreach (json_decode($results->data, true) as $result) 
				{
					if (is_array($result)) {
						foreach($result as $record){
							print '<li><a href="/researcher/search.php?LastName=&Department=0&ResearchInterest=' . $record . '&submit=Search">' . $record . '</a></li>';
						}
					}
				}
			}
		}
		print '</ul>';
	}
?>

<div class="LastName">
	
</div>