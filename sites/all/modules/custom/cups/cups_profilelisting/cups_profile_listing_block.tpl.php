<?php if($displayList) : ?>

<?php 
	$subDomainLink = ''; 
	$LinkVar = variable_get('cups_subdomain_link');

	
	global $base_url;

	if(isset($SiteName) && $SiteName != ''){
		if(isset($LinkVar) && $LinkVar != ''){
			$subDomainLink = '/' . $LinkVar;
		}
		$subDomainLink = $subDomainLink . '/' . $SiteName;
	}
	else
	{
		if(isset($LinkVar) && $LinkVar != ''){
			$subDomainLink = '/' . $LinkVar;
		}
	}

?>

<section class="profilelisting">
		<p class="searchstats">Listing for "<?php print variable_get('apiconfig_site_name'); ?>" site. <?php print $displayListResultsCount; ?> <?php print $ProfileListType; ?> profiles.</p>
		<?php foreach($ProfileListingList as &$ProfileListing): ?>
			<section class="resultscard" id="<?php print $ProfileListing['Id']; ?>">
			<!-------------------------------------------------------------->
			<?php if($profileDisplayType == 'showall' || $profileDisplayType == 'showall_researchinterests') { ?>
				<!-------------------------------------------------------------->
				<!-------------------------------------------------------------->
				<a class="profile_link" href="<?php print $subDomainLink; ?>/<?php print variable_get('cupsprofilepreview_subfolder_name'); ?>/<?php print $ProfileListing['ShortName']; ?><?php print $profileLinkType; ?>">
					<img class="headshot" alt="<?php print $ProfileListing['PersonName']['FullName']; ?>" src="<?php print $ProfileListing['HeadShotUrl']; ?>">
					<span>View Full Profile</span>
				</a>
				<!-------------------------------------------------------------->
				<h2><?php print $ProfileListing['PersonName']['FullName']; ?></h2>
				<!-------------------------------------------------------------->
			<?php
			if(variable_get('cups_researcherresults_person_academic_appointments')){
				$CupsPersonTitles = array_unique($ProfileListing['AcademicAppointments']);
			}
			if(isset($CupsPersonTitles) && count($CupsPersonTitles) > 0): ?>
			<section class="academic_appointments">
				<ul>
				<?php foreach($CupsPersonTitles as &$CupsPersonTitle): ?>
					<li><?php print $CupsPersonTitle; ?></li>
				<?php endforeach; ?>
				</ul>
			</section><!--titles-->
			<?php endif; ?>
			<!-------------------------------------------------------------->
			<?php
			if(variable_get('cups_researcherresults_administrative_titles')){
				$CupsPersonCurrentAdminTitles = array_unique($ProfileListing['AdministrativeTitles']);
			}
			if(isset($CupsPersonCurrentAdminTitles) && count($CupsPersonCurrentAdminTitles) > 0): ?>
			<section class="administrative_titles">
				<ul>
				<?php foreach($CupsPersonCurrentAdminTitles as &$CupsPersonCurrentAdminTitle): ?>
					<li><?php print $CupsPersonCurrentAdminTitle; ?></li>
				<?php endforeach; ?>
				</ul>
			</section><!--admin positions-->
			<?php endif; ?>
			<!-------------------------------------------------------------->
			<?php if($variables['profileListType'] == 'provider'){ ?>
			<?php if($ProfileListing['AcceptingNewPatients']) { ?>
				<?php if($ProfileListing['AppointmentPhoneNum'] != '') : ?>
					<span class="apptphone">This provider accepts new patients.<br/>Appointment Phone Number: <?php print $ProfileListing['AppointmentPhoneNum']; ?></span>
				<?php endif; ?>
			<?php } else { ?>
				<p>This provider does not accept new patients</p>
				<?php if($ProfileListing['ContactPhoneNum'] != '') : ?>
					<span class="apptphone">This provider does not accept new patients.<br/>Contact Phone Number: <?php print $ProfileListing['ContactPhoneNum']; ?></span>
				<?php endif; ?>
			<?php }} ?>
			<!-------------------------------------------------------------->
			<?php 
				if(variable_get('cups_researcherresults_research_interests')) : ?>
			<?php
			if(isset($ProfileListing['ResearchInterests']))
			{
				$CupsResearchInterests = array_unique($ProfileListing['ResearchInterests']);
			}
			if(isset($CupsResearchInterests) && count($CupsResearchInterests) > 0): ?>
			<section class="research_interests">
				<h3>Research Interests</h3>
				<ul>
				<?php foreach($CupsResearchInterests as &$CupsResearchInterest): ?>
					<li><?php print $CupsResearchInterest; ?></li>
				<?php endforeach; ?>
				</ul>
			</section><!--research interests-->
			<?php endif; ?>
			<?php endif; ?>
			<!-------------------------------------------------------------->
			<?php } else if ($profileDisplayType == 'listing_researchinterests') { ?>
			
			<h2><?php print $ProfileListing['PersonName']['FullName']; ?></h2>
			<a class="profile_link" href="/<?php print variable_get('cupsprofilepreview_subfolder_name'); ?>/<?php print $ProfileListing['ShortName']; ?><?php print $profileLinkType; ?>">
				<span>View Full Profile</span>
			</a>
			<!-------------------------------------------------------------->
			<?php 
				if(variable_get('cups_researcherresults_research_interests')) : ?>
			<?php
			if(isset($ProfileListing['ResearchInterests']))
			{
				$CupsResearchInterests = array_unique($ProfileListing['ResearchInterests']);
			}
			if(isset($CupsResearchInterests) && count($CupsResearchInterests) > 0): ?>
			<section class="research_interests">
				<h3>Research Interests</h3>
				<ul>
				<?php foreach($CupsResearchInterests as &$CupsResearchInterest): ?>
					<li><?php print $CupsResearchInterest; ?></li>
				<?php endforeach; ?>
				</ul>
			</section><!--research interests-->
			<?php endif; ?>
			<?php endif; ?>
			<!-------------------------------------------------------------->	

			<?php } else { ?>

			<h2><?php print $ProfileListing['PersonName']['FullName']; ?></h2>
			<a class="profile_link" href="/<?php print variable_get('cupsprofilepreview_subfolder_name'); ?>/<?php print $ProfileListing['ShortName']; ?><?php print $profileLinkType; ?>">
				<span>View Full Profile</span>
			</a>
			<!-------------------------------------------------------------->
			
			<?php } ?>
			</section>
		<?php endforeach; ?>
</section>
<?php endif; ?>
