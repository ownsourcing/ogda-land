<?php

function cups_profilelisting_settings() {
	//--------------------------------------------------------------
	$form['cups_search_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Search Settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['cups_search_results_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Search Results Settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_faculty_search_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Faculty Search Settings'),
		'#description' => t('Settings page for CUPS Faculty search. Show/hide available fields for search form below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_provider_search_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Provider Search Settings'),
		'#description' => t('Settings page for CUPS Provider search. Show/hide available fields for search form below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_researcher_search_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Researcher Search Settings'),
		'#description' => t('Settings page for CUPS Researcher search. Show/hide available fields for search form below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_facultyresults_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Faculty Results Settings'),
		'#description' => t('Settings page for CUPS search results. Show/hide available fields within each section for the search results page below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------

	$form['cups_search_results_settings']['cups_providerresults_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Provider Results Settings'),
		'#description' => t('Settings page for CUPS search results. Show/hide available fields within each section for the search results page below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_researcherresults_page'] = array(
		'#type' => 'fieldset',
		'#title' => t('CUPS Researcher Results Settings'),
		'#description' => t('Settings page for CUPS search results. Show/hide available fields within each section for the search results page below.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_faculty_search_page']['cups_faculty_search_byname'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Name'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_faculty_search_byname', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_settings']['cups_faculty_search_page']['cups_faculty_search_bydepartment'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Department'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_faculty_search_bydepartment', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_faculty_search_page']['cups_faculty_search_listall'] = array(
		'#type' => 'checkbox',
		'#title' => t('List All Link'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_faculty_search_listall', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_byname'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Name'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_provider_search_byname', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_byspecialty'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Specialty'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_byspecialty', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_bydepartment'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Department'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_bydepartment', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_byispeds'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Is Pediatrics'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_byispeds', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_byinsurance'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Insurance'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_byinsurance', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_bygender'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Gender'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_bygender', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_bylanguage'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Language'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_bylanguage', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_bylocation'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Location'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_bylocation', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_byzip'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Zip'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_byzip', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_listall'] = array(
		'#type' => 'checkbox',
		'#title' => t('List All Link'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_listall', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_moreoptions'] = array(
		'#type' => 'checkbox',
		'#title' => t('More Options Link'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_provider_search_moreoptions', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	/*
	$form['cups_search_settings']['cups_provider_search_page']['cups_provider_search_bydepartment_default_value'] = array(
		'#type' => 'textfield',
		'#title' => t('Default Department ID'),
		'#default_value' => variable_get('cups_provider_search_bydepartment_default_value', ''),
		'#description' => t('Default department ID if you want specific department results.'),
		'#required' => FALSE,
	);
	*/
	//--------------------------------------------------------------
	$form['cups_search_settings']['cups_researcher_search_page']['cups_researcher_search_byname'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Name'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_researcher_search_byname', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_settings']['cups_researcher_search_page']['cups_researcher_search_bydepartment'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Department'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_researcher_search_bydepartment', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_researcher_search_page']['cups_researcher_search_byresearchinterests'] = array(
		'#type' => 'checkbox',
		'#title' => t('Search By Research Interests'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_researcher_search_byresearchinterests', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	$form['cups_search_settings']['cups_researcher_search_page']['cups_researcher_search_listall'] = array(
		'#type' => 'checkbox',
		'#title' => t('List All Link'),
		'#description' => t('Optional Field'),
		'#default_value' => variable_get('cups_researcher_search_listall', 0),
		'#required' => FALSE,
		'#disabled' => FALSE,
	);
	//--------------------------------------------------------------
	//--------------------------------------------------------------
	//--------------------------------------------------------------Person
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_facultyresults_page']['cups_facultyresults_person_name'] = array(
		'#type' => 'checkbox',
		'#title' => t('Full Name (First, Middle, Last, Name Suffix, Professional Suffix)'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_facultyresults_person_name', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_facultyresults_page']['cups_facultyresults_person_headshot'] = array(
		'#type' => 'checkbox',
		'#title' => t('Headshot'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_facultyresults_person_headshot', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_facultyresults_page']['cups_facultyresults_person_dpt_div'] = array(
		'#type' => 'checkbox',
		'#title' => t('Department(s)/Division(s)'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_facultyresults_person_dpt_div', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_facultyresults_page']['cups_facultyresults_person_academic_appointments'] = array(
		'#type' => 'checkbox',
		'#title' => t('Academic Appointments'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_facultyresults_person_academic_appointments', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_facultyresults_page']['cups_facultyresults_administrative_titles'] = array(
		'#type' => 'checkbox',
		'#title' => t('Administrative Titles'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_facultyresults_administrative_titles', 0),
		'#required' => FALSE,
	);
	//--------------------------------------------------------------
	//--------------------------------------------------------------
	//--------------------------------------------------------------Person
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_person_name'] = array(
		'#type' => 'checkbox',
		'#title' => t('Full Name (First, Middle, Last, Name Suffix, Professional Suffix)'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_researcherresults_person_name', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_person_headshot'] = array(
		'#type' => 'checkbox',
		'#title' => t('Headshot'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_researcherresults_person_headshot', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_person_dpt_div'] = array(
		'#type' => 'checkbox',
		'#title' => t('Department(s)/Division(s)'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_researcherresults_person_dpt_div', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_person_academic_appointments'] = array(
		'#type' => 'checkbox',
		'#title' => t('Academic Appointments'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_researcherresults_person_academic_appointments', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_administrative_titles'] = array(
		'#type' => 'checkbox',
		'#title' => t('Administrative Titles'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_researcherresults_administrative_titles', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_researcherresults_page']['cups_researcherresults_research_interests'] = array(
		'#type' => 'checkbox',
		'#title' => t('Research Interests'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_researcherresults_research_interests', 0),
		'#required' => FALSE,
	);
	//--------------------------------------------------------------
	//--------------------------------------------------------------
	//--------------------------------------------------------------Person
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_person_name'] = array(
		'#type' => 'checkbox',
		'#title' => t('Full Name (First, Middle, Last, Name Suffix, Professional Suffix)'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_providerresults_person_name', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_person_headshot'] = array(
		'#type' => 'checkbox',
		'#title' => t('Headshot'),
		'#description' => t('This field is mandatory and cannot be disabled'),
		'#default_value' => variable_get('cups_providerresults_person_headshot', 1),
		'#required' => TRUE,
		'#disabled' => TRUE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_person_dpt_div'] = array(
		'#type' => 'checkbox',
		'#title' => t('Department(s)/Division(s)'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_person_dpt_div', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_person_academic_appointments'] = array(
		'#type' => 'checkbox',
		'#title' => t('Academic Appointments'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_person_academic_appointments', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_administrative_titles'] = array(
		'#type' => 'checkbox',
		'#title' => t('Administrative Titles'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_administrative_titles', 0),
		'#required' => FALSE,
	);
	//--------------------------------------------------------------
	//--------------------------------------------------------------Provider
	//--------------------------------------------------------------
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_boards'] = array(
		'#type' => 'checkbox',
		'#title' => t('Boards (specialties)'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_boards', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_expertise'] = array(
		'#type' => 'checkbox',
		'#title' => t('Clinical Expertise'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_expertise', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_locations'] = array(
		'#type' => 'checkbox',
		'#title' => t('Locations'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_locations', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_accept_new_patients'] = array(
		'#type' => 'checkbox',
		'#title' => t('Accepting new patients'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_accept_new_patients', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_usa_top_doc'] = array(
		'#type' => 'checkbox',
		'#title' => t('Americas Top Doctor'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_usa_top_doc', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_ny_top_doc'] = array(
		'#type' => 'checkbox',
		'#title' => t('New York Top Doctor'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_ny_top_doc', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_app_phone_num'] = array(
		'#type' => 'checkbox',
		'#title' => t('Appointment Phone Number'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_app_phone_num', 0),
		'#required' => FALSE,
	);
	$form['cups_search_results_settings']['cups_providerresults_page']['cups_providerresults_provider_pracs_and_centers'] = array(
		'#type' => 'checkbox',
		'#title' => t('Practices And Centers'),
		'#description' => t('This field is optional'),
		'#default_value' => variable_get('cups_providerresults_provider_pracs_and_centers', 0),
		'#required' => FALSE,
	);
	//--------------------------------------------------------------
	return system_settings_form($form);
}
