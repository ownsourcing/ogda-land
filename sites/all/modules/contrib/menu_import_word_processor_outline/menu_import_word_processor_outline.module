<?php
/**
 * @file
 * Module's main file, general definitions and hooks.
 */

/**
 * Implementation of hook_help().
 */
function menu_import_word_processor_outline_help($path, $arg) {
  $output = '';

  switch ($path) {
    case 'admin/help/menu_import':
      $output .= '<p>';
      $output .= t('<strong>Menu Import from Outline in Word Processor Document</strong> extends Menu Import module to allow you to import a menu hierarchy from a Microsoft Word .docx (XML) file. This is for creating an initial content. Word document files are expected to have an outline structure like the attached:');
      $output .= '</p>';
      break;
  }

  return $output;
}

/**
 * Implementation of hook_form_alter().
 */
function menu_import_word_processor_outline_form_menu_import_form_alter(&$form, $form_state) {
  $step = isset($form_state['storage']['step']) ? $form_state['storage']['step'] : 1;
  if ($step == 1) {
    // This typo is theirs, when they fix it, this will break... @TODO file bug.
    // unset($form['actions']['sumbit']['#validate']['menu_import_form_step1_validate']);
    array_unshift($form['actions']['sumbit']['#validate'], 'menu_import_word_processor_outline_form_step1_validate');
  }
}

/**
 * Validation for docx files.
 */
function menu_import_word_processor_outline_form_step1_validate($form, &$form_state) {
  $node_types = node_type_get_names();
  if ($form_state['values']['create_content'] && empty($node_types)) {
    form_set_error('create_content', t('No content types were found, cannot create initial content.'));
  }

  $validators = array(
    'file_validate_extensions' => array('docx', 'odt', 'txt'),
  );

  if (empty($form_state['values']['text'])) {
    $file = file_save_upload('upload', $validators);
    if (!$file) {
      form_set_error('upload', t('You must select a valid text file to upload.'));
    }
    else {
      $form_state['values']['file'] = $file;

      // Unset the txt validation and submission.
      // $key = array_search('menu_import_form_step1_validate', $form_state['#validate']);
      // unset($form_state['#validate'][$key]);
      $key = array_search('menu_import_form_step1_submit', $form_state['clicked_button']['#submit']);
      $form_state['clicked_button']['#submit'][$key] = 'menu_import_word_processor_outline_form_step1_submit';
      $key = array_search('menu_import_form_step1_submit', $form_state['submit_handlers']);
      $form_state['submit_handlers'][$key] = 'menu_import_word_processor_outline_form_step1_submit';
      $key = array_search('menu_import_form_step1_submit', $form_state['buttons'][0]['#submit']);
      // don't need to find this it seems: debug($form_state['import_source']['actions']);
      // $form_state['buttons'][0]['#submit'][$key] = 'menu_import_word_processor_outline_form_step1_submit';
      // Note their typo again.
      // $key = array_search('menu_import_form_step1_submit', $form_state['import_source']['actions']['sumbit']['#submit']);
      $form_state['import_source']['actions']['sumbit']['#submit'][$key] = 'menu_import_word_processor_outline_form_step1_submit';
    }
  }
}

/**
 * Step 1 form submit handler.
 */
function menu_import_word_processor_outline_form_step1_submit($form, &$form_state) {
  $form_state['storage']['options'] = array(
    'parent_mlid' => $form_state['values']['parent_mlid'],
    'create_content' => $form_state['values']['create_content'],
    'link_to_content' => $form_state['values']['link_to_content'],
    'remove_menu_items' => $form_state['values']['remove_menu_items'],
    'language' => $form_state['values']['language'],
    'node_type' => $form_state['values']['node_type'],
    'node_body' => $form_state['values']['node_body'],
    'node_format' => $form_state['values']['node_format'],
    'node_author' => $form_state['values']['node_author'],
    'node_status' => $form_state['values']['node_status'],
    'node_alias' => $form_state['values']['node_alias'],
  );

  module_load_include('inc', 'menu_import', 'includes/import');

  $menu_name = $form_state['values']['menu_name'];
  $options = $form_state['storage']['options'];
  $file = $form_state['values']['file'];
  $menu = menu_import_word_processor_outline_parse_menu_from_file($file->uri, $menu_name, $options);
  file_delete($file);

  if (!empty($menu['errors'])) {
    foreach ($menu['errors'] as $error) {
      drupal_set_message(check_plain($error), 'error');
    }
  }

  if (!empty($menu['warnings'])) {
    foreach ($menu['warnings'] as $warn) {
      drupal_set_message($warn, 'warning');
    }
  }

  $form_state['storage']['menu'] = $menu;
  $form_state['storage']['step'] = 2;
  $form_state['rebuild'] = TRUE;
  $form_state['page_values'][1] = $form_state['values'];
}


/**
 * File parser function. Reads through the text file and constructs the menu.
 *
 * @param $uri
 *   uri of the uploaded file.
 * @param $menu_name
 *   internal name of existiong menu.
 * @param $options
 *   An associative array of search options.
 *   - search_title: search node by title.
 *
 * @return array
 *   array structure of menu.
 */
function menu_import_word_processor_outline_parse_menu_from_file($uri, $menu_name, array $options) {
  $menu = array(
    'errors'    => array(),
    'warnings'  => array(),
    0 => array(
      'menu_name' => $menu_name,
      'children' => array(),
    )
  );

  // Keep track of actual weights per level.
  // We have to append to existing items not to mess up the menu.
  $weights = array(0 => menu_import_get_max_weight($menu_name));
  // Keep track of actual parents per level.
  $parents = array();

  $xml_content = array(
    'content.xml',
    'word/document.xml',
  );
  $data = '';
  $doc = zip_open(file_stream_wrapper_get_instance_by_uri($uri)->realpath());
  while ($doc_item = zip_read($doc)) {
    if (zip_entry_open($doc, $doc_item) && in_array(zip_entry_name($doc_item), $xml_content)) {
      $data .= zip_entry_read($doc_item, zip_entry_filesize($doc_item));
      zip_entry_close($doc_item);
    }
  }
  zip_close($doc);

  $mimetype = file_stream_wrapper_get_instance_by_uri($uri)->getMimeType($uri);
  $xml = simplexml_load_string($data, "SimpleXMLElement", 0, 'w', TRUE);

  if ($xml === FALSE) {
    $menu['errors'][] = t("Couldn't open the uploaded file for reading as XML.");
    return $menu;
  }

  $paragraphs = $xml->xpath('/w:document/w:body/w:p[w:pPr/w:numPr/w:ilvl/@w:val]');

  $level = $current_line = 0;
  foreach ($paragraphs as $p) {
    $current_line++;

    // Note: The xpath parsing should already skip 'empty lines'.

    $menuitem = menu_import_word_processor_outline_parse_line($p, $level, $weights, $parents, $options);
    if ($menuitem['error']) {
      $menu['errors'][] = t('Error on item @line_number: @error.', array('@line_number' => $current_line, '@error' => $menuitem['error']));
      $current_line--;
    }
    else {
      $menu[$current_line] = $menuitem;
      $menu[$menuitem['parent']]['children'][] = $current_line;

      $level = $menuitem['level'];
      $parents[$level] = $current_line;
      $weights[$level] = $menuitem['weight'];
    }
  }

  return $menu;
}

/**
 * Parse a section of XML containing the structure of one menu link.
 *
 * @param $line
 *   One item from input file.
 * @param $prev_level
 *   Previous level to build hierarchy.
 * @param $weights
 *   Array of menu items' weights.
 * @param $parents
 *   Array of menu items' parents.
 * @param $options
 *   Array of importing options.
 *
 * @return
 *   Array representing a menu item.
 */
function menu_import_word_processor_outline_parse_line($line, $prev_level, array $weights, array $parents, array $options) {
  $menuitem = array(
    'error' => FALSE,
    'link_title' => NULL,
    'children' => array(),
    'parent' => 0,
    'nid' => FALSE,
    'path' => FALSE,
    'weight' => 0,
    'external' => FALSE,
    'level' => 0,
    'options' => array(),
  );

  // Set default language
  if (module_exists('i18n_menu')) {
    $menuitem['language'] = $options['language'];
  }
  $langs = array_keys(language_list());

  $path = $description = $expanded = $hidden = '';
  $language = NULL;

  $ilvl_val = $line->xpath('w:pPr/w:numPr/w:ilvl/@w:val');
  $indent = (string) $ilvl_val[0];

  $t = $line->xpath('w:r/w:t');
  $title = (string) $t[0];

  $level_title = array($indent, $title);

  // TODO actually check level and title are being gotton
  if (!$level_title) {
    return _menu_import_mark_error_item($menuitem, t('missing title or wrong indentation'), $line);
  }
  else {
    list($level, $title) = $level_title;
  }

  // Skip empty items
  if (!strlen($title)) {
    return _menu_import_mark_error_item($menuitem, t('missing item title'), $line);
  }

  // Make sure this item is only 1 level below the last item.
  if ($level > $prev_level + 1) {
    return _menu_import_mark_error_item($menuitem, t('wrong indentation'), $line);
  }

  if (isset($weights[$level])) {
    if ($level > $prev_level) {
      $weight = 0;
    }
    else {
      $weight = $weights[$level] + 1;
    }
  }
  else {
    $weight = 0;
  }
  $menuitem['weight'] = $weight;
  $menuitem['parent'] = !$level ? 0 : $parents[$level - 1];
  $menuitem['link_title'] = $title;
  $menuitem['level'] = $level;
  $menuitem['path'] = $path;

  if (!empty($link_options)) {
    $menuitem['options'] = $link_options;
  }

  if (url_is_external($path)) {
    $menuitem['external'] = TRUE;
    $menuitem['link_path'] = $path;
  }
  elseif (!empty($node_uuid)) {
    $result = entity_get_id_by_uuid('node', array($node_uuid));
    $menuitem['link_path'] = 'node/' . $result[$node_uuid];
    $menuitem['nid'] = $result[$node_uuid];
  }
  else {
    $result = _menu_import_lookup_path($path, $title, $language, $options);
    $menuitem['link_path'] = $result['link_path'];
    $menuitem['nid'] = $result['nid'];
    $menuitem['node_view'] = $result['node_view'];
    if ($result['options']) {
      $menuitem['options'] = array_merge($menuitem['options'], $result['options']);
    }
  }

  if ($description) {
    $menuitem['description'] = $description;
  }
  if ($hidden) {
    $menuitem['hidden'] = 1;
  }
  if ($expanded) {
    $menuitem['expanded'] = 1;
  }
  if ($language) {
    $menuitem['language'] = $language;
    // Important when setting the language, otherwise it'll be ignored.
    $menuitem['customized'] = 1;
  }

  return $menuitem;
}


/**
 * Import menu from docx file.
 *
 * @param $uri
 *   uri of the uploaded file.
 * @param $menu_name
 *   iternal name of the menu.
 * @param $options
 *   An associative array of import options. See menu_import_save_menu for reference.
 * @see menu_import_save_menu
 *
 * @return array
 *   An associative array of result.
 *   - error: in case of error, this will contain an array of error messages;
 *   - deleted_nodes: count of nodes deleted;
 *   - matched_nodes: count of nodes matched;
 *   - new_nodes: count of nodes created;
 *   - unknown_links: count of menu items with internal links (not nodes);
 *   - external_links: count of menu items with external links.
 */
function menu_import_word_processor_outline_file($uri, $menu_name, array $options) {
  // module_load_include('inc', 'menu_import_word_processor_outline', 'includes/import');
  module_load_include('inc', 'menu_import', 'includes/import');
  $menu = menu_import_word_processor_outline_parse_menu_from_file($uri, $menu_name, $options);
  // Stop import on any errors.
  if ($menu['errors']) {
    return array('errors' => $menu['errors']);
  }

  $result = menu_import_save_menu($menu, $options);

  if (!empty($menu['warnings'])) {
    $result['warnings'] = $menu['warnings'];
  }

  return $result;
}


/**
 * Different options for .docx versus .odt files.
 */
function _menu_import_word_processor_outline_set_xml_options($mimetype = 'all') {
  $o = array(
    'docx' => array(
      'ns' => 'w',
      'item_xpath' => '/w:document/w:body/w:p[w:pPr/w:numPr/w:ilvl/@w:val]',
      'title_xpath' => 'w:r/w:t',
      'indent_xpath' => 'w:pPr/w:numPr/w:ilvl/@w:val',
    ),
    'odt' => array(
      'ns' => 'office',
      'item_xpath' => '',
      'title_xpath' => '',
      'indent_xpath' => NULL,
    ),
  );
  if ($mimetype == 'all') {
    return $o;
  }
  elseif (array_key_exists($mimetype, $o)) {
    return $o[$mimetype];
  }
  else {
    return array();
  }
}
